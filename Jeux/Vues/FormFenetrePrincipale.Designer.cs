﻿namespace Jeux.vues
{
    partial class FormFenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFenetrePrincipale));
            this.buttonSnake = new System.Windows.Forms.Button();
            this.buttonPingPong = new System.Windows.Forms.Button();
            this.buttonPendu = new System.Windows.Forms.Button();
            this.buttonMorpion = new System.Windows.Forms.Button();
            this.labelTitre = new System.Windows.Forms.Label();
            this.menuStripMenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monCompteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableauDesScoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personnaliserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.àproposdeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBoxPseudo = new System.Windows.Forms.ToolStripTextBox();
            this.menuStripMenuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSnake
            // 
            this.buttonSnake.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSnake.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSnake.Location = new System.Drawing.Point(322, 520);
            this.buttonSnake.Name = "buttonSnake";
            this.buttonSnake.Size = new System.Drawing.Size(377, 55);
            this.buttonSnake.TabIndex = 12;
            this.buttonSnake.Text = "SNAKE";
            this.buttonSnake.UseVisualStyleBackColor = false;
            this.buttonSnake.Click += new System.EventHandler(this.buttonSnake_Click);
            // 
            // buttonPingPong
            // 
            this.buttonPingPong.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonPingPong.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPingPong.Location = new System.Drawing.Point(322, 410);
            this.buttonPingPong.Name = "buttonPingPong";
            this.buttonPingPong.Size = new System.Drawing.Size(377, 55);
            this.buttonPingPong.TabIndex = 11;
            this.buttonPingPong.Text = "PING PONG";
            this.buttonPingPong.UseVisualStyleBackColor = false;
            this.buttonPingPong.Click += new System.EventHandler(this.buttonPingPong_Click);
            // 
            // buttonPendu
            // 
            this.buttonPendu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonPendu.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPendu.Location = new System.Drawing.Point(322, 308);
            this.buttonPendu.Name = "buttonPendu";
            this.buttonPendu.Size = new System.Drawing.Size(377, 56);
            this.buttonPendu.TabIndex = 9;
            this.buttonPendu.Text = "PENDU";
            this.buttonPendu.UseVisualStyleBackColor = false;
            this.buttonPendu.Click += new System.EventHandler(this.ButtonPendu_Click);
            // 
            // buttonMorpion
            // 
            this.buttonMorpion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonMorpion.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMorpion.Location = new System.Drawing.Point(322, 203);
            this.buttonMorpion.Name = "buttonMorpion";
            this.buttonMorpion.Size = new System.Drawing.Size(377, 56);
            this.buttonMorpion.TabIndex = 8;
            this.buttonMorpion.Text = "MORPION";
            this.buttonMorpion.UseVisualStyleBackColor = false;
            this.buttonMorpion.Click += new System.EventHandler(this.ButtonMorpion_Click);
            // 
            // labelTitre
            // 
            this.labelTitre.AutoSize = true;
            this.labelTitre.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitre.Location = new System.Drawing.Point(382, 38);
            this.labelTitre.Name = "labelTitre";
            this.labelTitre.Size = new System.Drawing.Size(276, 119);
            this.labelTitre.TabIndex = 7;
            this.labelTitre.Text = "JEUX";
            this.labelTitre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuStripMenuPrincipal
            // 
            this.menuStripMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.outilsToolStripMenuItem,
            this.aideToolStripMenuItem,
            this.toolStripTextBoxPseudo});
            this.menuStripMenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuStripMenuPrincipal.Name = "menuStripMenuPrincipal";
            this.menuStripMenuPrincipal.Size = new System.Drawing.Size(1021, 27);
            this.menuStripMenuPrincipal.TabIndex = 14;
            this.menuStripMenuPrincipal.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monCompteToolStripMenuItem,
            this.quitterToolStripMenuItem,
            this.tableauDesScoresToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 23);
            this.fichierToolStripMenuItem.Text = "&Fichier";
            // 
            // monCompteToolStripMenuItem
            // 
            this.monCompteToolStripMenuItem.Name = "monCompteToolStripMenuItem";
            this.monCompteToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.monCompteToolStripMenuItem.Text = "Mon compte";
            this.monCompteToolStripMenuItem.Click += new System.EventHandler(this.monCompteToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // tableauDesScoresToolStripMenuItem
            // 
            this.tableauDesScoresToolStripMenuItem.Name = "tableauDesScoresToolStripMenuItem";
            this.tableauDesScoresToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.tableauDesScoresToolStripMenuItem.Text = "Tableau des scores";
            this.tableauDesScoresToolStripMenuItem.Click += new System.EventHandler(this.tableauDesScoresToolStripMenuItem_Click);
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.personnaliserToolStripMenuItem});
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(50, 23);
            this.outilsToolStripMenuItem.Text = "&Outils";
            // 
            // personnaliserToolStripMenuItem
            // 
            this.personnaliserToolStripMenuItem.Name = "personnaliserToolStripMenuItem";
            this.personnaliserToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.personnaliserToolStripMenuItem.Text = "&Personnaliser";
            // 
            // aideToolStripMenuItem
            // 
            this.aideToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator5,
            this.àproposdeToolStripMenuItem});
            this.aideToolStripMenuItem.Name = "aideToolStripMenuItem";
            this.aideToolStripMenuItem.Size = new System.Drawing.Size(43, 23);
            this.aideToolStripMenuItem.Text = "&Aide";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(144, 6);
            // 
            // àproposdeToolStripMenuItem
            // 
            this.àproposdeToolStripMenuItem.Name = "àproposdeToolStripMenuItem";
            this.àproposdeToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.àproposdeToolStripMenuItem.Text = "À &propos de...";
            // 
            // toolStripTextBoxPseudo
            // 
            this.toolStripTextBoxPseudo.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.toolStripTextBoxPseudo.Enabled = false;
            this.toolStripTextBoxPseudo.Name = "toolStripTextBoxPseudo";
            this.toolStripTextBoxPseudo.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBoxPseudo.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FormFenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1021, 651);
            this.Controls.Add(this.buttonSnake);
            this.Controls.Add(this.buttonPingPong);
            this.Controls.Add(this.buttonPendu);
            this.Controls.Add(this.buttonMorpion);
            this.Controls.Add(this.labelTitre);
            this.Controls.Add(this.menuStripMenuPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripMenuPrincipal;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFenetrePrincipale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormFenetrePrincipale";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormFenetrePrincipale_FormClosed);
            this.Load += new System.EventHandler(this.FormFenetrePrincipale_Load);
            this.menuStripMenuPrincipal.ResumeLayout(false);
            this.menuStripMenuPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSnake;
        private System.Windows.Forms.Button buttonPingPong;
        private System.Windows.Forms.Button buttonPendu;
        private System.Windows.Forms.Button buttonMorpion;
        private System.Windows.Forms.Label labelTitre;
        private System.Windows.Forms.MenuStrip menuStripMenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monCompteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personnaliserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem àproposdeToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxPseudo;
        private System.Windows.Forms.ToolStripMenuItem tableauDesScoresToolStripMenuItem;
    }
}