﻿namespace Jeux.vues
{
    partial class FormConnexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelConnexion = new System.Windows.Forms.Label();
            this.buttonInscription = new System.Windows.Forms.Button();
            this.buttonConnexion = new System.Windows.Forms.Button();
            this.textBoxMotDePasse = new System.Windows.Forms.TextBox();
            this.textBoxPseudo = new System.Windows.Forms.TextBox();
            this.labelMotDePasseUtilisateur = new System.Windows.Forms.Label();
            this.labelNomUtilisateur = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelConnexion
            // 
            this.labelConnexion.AutoSize = true;
            this.labelConnexion.Font = new System.Drawing.Font("Berlin Sans FB", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnexion.Location = new System.Drawing.Point(317, 9);
            this.labelConnexion.Name = "labelConnexion";
            this.labelConnexion.Size = new System.Drawing.Size(425, 79);
            this.labelConnexion.TabIndex = 19;
            this.labelConnexion.Text = "CONNEXION";
            // 
            // buttonInscription
            // 
            this.buttonInscription.AutoSize = true;
            this.buttonInscription.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInscription.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInscription.Location = new System.Drawing.Point(225, 442);
            this.buttonInscription.Name = "buttonInscription";
            this.buttonInscription.Size = new System.Drawing.Size(256, 53);
            this.buttonInscription.TabIndex = 25;
            this.buttonInscription.Text = "INSCRIPTION";
            this.buttonInscription.UseVisualStyleBackColor = false;
            this.buttonInscription.Click += new System.EventHandler(this.ButtonInscription_Click);
            // 
            // buttonConnexion
            // 
            this.buttonConnexion.AutoSize = true;
            this.buttonConnexion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonConnexion.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnexion.Location = new System.Drawing.Point(715, 442);
            this.buttonConnexion.Name = "buttonConnexion";
            this.buttonConnexion.Size = new System.Drawing.Size(234, 53);
            this.buttonConnexion.TabIndex = 24;
            this.buttonConnexion.Text = "CONNEXION";
            this.buttonConnexion.UseVisualStyleBackColor = false;
            this.buttonConnexion.Click += new System.EventHandler(this.ButtonConnexion_Click);
            // 
            // textBoxMotDePasse
            // 
            this.textBoxMotDePasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMotDePasse.Location = new System.Drawing.Point(396, 299);
            this.textBoxMotDePasse.Name = "textBoxMotDePasse";
            this.textBoxMotDePasse.PasswordChar = '*';
            this.textBoxMotDePasse.Size = new System.Drawing.Size(346, 40);
            this.textBoxMotDePasse.TabIndex = 23;
            this.textBoxMotDePasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxMotDePasse.Leave += new System.EventHandler(this.textBoxMotDePasse_Leave);
            // 
            // textBoxPseudo
            // 
            this.textBoxPseudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPseudo.Location = new System.Drawing.Point(398, 185);
            this.textBoxPseudo.Name = "textBoxPseudo";
            this.textBoxPseudo.Size = new System.Drawing.Size(346, 40);
            this.textBoxPseudo.TabIndex = 22;
            this.textBoxPseudo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPseudo.Leave += new System.EventHandler(this.TextBoxNomUitlisateur_Leave);
            // 
            // labelMotDePasseUtilisateur
            // 
            this.labelMotDePasseUtilisateur.AutoSize = true;
            this.labelMotDePasseUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotDePasseUtilisateur.Location = new System.Drawing.Point(390, 252);
            this.labelMotDePasseUtilisateur.Name = "labelMotDePasseUtilisateur";
            this.labelMotDePasseUtilisateur.Size = new System.Drawing.Size(277, 44);
            this.labelMotDePasseUtilisateur.TabIndex = 21;
            this.labelMotDePasseUtilisateur.Text = "MOT DE PASSE:";
            // 
            // labelNomUtilisateur
            // 
            this.labelNomUtilisateur.AutoSize = true;
            this.labelNomUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomUtilisateur.Location = new System.Drawing.Point(390, 138);
            this.labelNomUtilisateur.Name = "labelNomUtilisateur";
            this.labelNomUtilisateur.Size = new System.Drawing.Size(170, 44);
            this.labelNomUtilisateur.TabIndex = 20;
            this.labelNomUtilisateur.Text = "PSEUDO:";
            // 
            // FormConnexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1075, 535);
            this.Controls.Add(this.buttonInscription);
            this.Controls.Add(this.buttonConnexion);
            this.Controls.Add(this.textBoxMotDePasse);
            this.Controls.Add(this.textBoxPseudo);
            this.Controls.Add(this.labelMotDePasseUtilisateur);
            this.Controls.Add(this.labelNomUtilisateur);
            this.Controls.Add(this.labelConnexion);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConnexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormConnexion";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormConnexion_FormClosed);
            this.Load += new System.EventHandler(this.FormConnexion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConnexion;
        private System.Windows.Forms.Button buttonInscription;
        private System.Windows.Forms.Button buttonConnexion;
        private System.Windows.Forms.TextBox textBoxMotDePasse;
        private System.Windows.Forms.TextBox textBoxPseudo;
        private System.Windows.Forms.Label labelMotDePasseUtilisateur;
        private System.Windows.Forms.Label labelNomUtilisateur;
    }
}