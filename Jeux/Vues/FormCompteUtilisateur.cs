﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Jeux.vues;

namespace Jeux.Vues
{
    public partial class FormCompteUtilisateur : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        static string pseudo;
        Form MdiChild;
        public FormCompteUtilisateur()
        {
            InitializeComponent();
        }

        private void FormCompteUtilisateur_Load(object sender, EventArgs e)
        {
             
            sqlConnection = new MySqlConnection(connection);
            sqlConnection.Open();
            textBoxPseudoUtilisateur.Text = pseudo;
            //MySqlDataAdapter sda = new MySqlDataAdapter("Select * from PERSONNE where pseudo='" + textBoxPseudoUtilisateur.Text + "'", sqlConnection);
            //DataTable dataTable = new DataTable();
            MySqlCommand sql = new MySqlCommand("Select * from PERSONNE where pseudo = '" + textBoxPseudoUtilisateur.Text + "'");
            sql.Connection = sqlConnection;
            IDataReader reader =  sql.ExecuteReader();
            while (reader.Read())
            {
                textBoxNomUtilisateur.Text = Convert.ToString(reader.GetValue(1));
                textBoxMotDePasseUtilisateur.Text = Convert.ToString(reader.GetValue(3));
            }
            sqlConnection.Close();
        }

        public static void SetPseudo(string pseudoJoueur)
        {
            pseudo = pseudoJoueur;
        }

        private void ButtonModifier_Click(object sender, EventArgs e)
        {
            MdiChild = new FormModifierUtilisateur();
            MdiChild.Show();
            this.Hide();
        }

        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            sqlConnection = new MySqlConnection(connection);
            sqlConnection.Open();
            MySqlCommand sql = new MySqlCommand("Delete from PERSONNE where pseudo = '" + textBoxPseudoUtilisateur.Text + "'");
            sql.Connection = sqlConnection;
            sql.ExecuteNonQuery();
            MessageBox.Show("Votre compte a été supprimé");
            MdiChild = new FormConnexion();
            MdiChild.Show();
            this.Hide();
            sqlConnection.Close();
        }

        private void FormCompteUtilisateur_FormClosed(object sender, FormClosedEventArgs e)
        {
            MdiChild = new FormFenetrePrincipale();
            MdiChild.Show();
            this.Hide();
        }
    }
}
