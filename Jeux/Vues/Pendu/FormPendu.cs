﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using System.IO;

namespace Jeux.Vues.Pendu
{
    public partial class FormPendu : Form
    {
        #region VARIABLES
        Form MdiChild;

        static string utilisateur;

        /// <summary>
        /// Chemin du dictionnaire
        /// </summary>
        string cheminFichier = Environment.CurrentDirectory + @"/dico.txt";

        /// <summary>
        /// Chemin des images
        /// </summary>
        string cheminImage = Environment.CurrentDirectory + @"/partie";

        /// <summary>
        /// Numero d'image
        /// </summary>
        int numeroImage = -1;

        /// <summary>
        /// Collection des mots du dictionnaire
        /// </summary>
        List<string> collectionMots = new List<string>();

        /// <summary>
        /// Collection des mots du dictionnaire
        /// </summary>
        List<string> collectionLettreSaisie = new List<string>();

        /// <summary>
        /// Mot à trouver 
        /// </summary>
        string motATrouver;

        /// <summary>
        /// Mot en cours de recherche
        /// </summary>
        string motEnCours;

        /// <summary>
        /// Concerne la musique à lancer
        /// </summary>
        WindowsMediaPlayer musique = new WindowsMediaPlayer();

        /// <summary>
        /// Concerne la lettre saisie par l'utilisateur
        /// </summary>
        string lettreSaisie = "";

        /// <summary>
        /// On recrer le motEnCours
        /// </summary>
        string motCourant = "";
        #endregion

        public FormPendu()
        {
            musique.URL = "bach.mp3";
            if (!File.Exists(cheminFichier))
            {
                DialogResult resultat = MessageBox.Show("ERREUR: ne trouve pas le fichier demandé");
                Environment.Exit(1);
            }
            collectionMots = File.ReadAllLines(cheminFichier).ToList();
            NouveauJeuToolStripMenuItem_Click(new object(), new EventArgs());
            InitializeComponent();
        }

        /// <summary>
        /// Méthode de chargement de mon formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPendu_Load(object sender, EventArgs e)
        {
            musique.controls.play();
            labelUtilisateur.Text = utilisateur;
        }

        /// <summary>
        /// Méthode d'affichage d'un message concernant le jeu du pendu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Par Anouk ZOUAOUI\nBTS SIO SLAM\n lycee Jules Ferry\nMusique: Prélude, interprété par Anouk ZOUAOUI, BACH", "A propos du pendu");
        }

        /// <summary>
        /// Méthode de la reinitialisation du jeu lors s'une nouvelle partie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NouveauJeuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Random rdm = new Random();
            motATrouver = collectionMots[rdm.Next(0, collectionMots.Count)].ToUpper().Replace(" ", "");
            motEnCours = "";
            labelMot.Text = "";
            if (motATrouver.Length < 6)
            {
                for (int i = 0; i < motATrouver.Length; i++)
                {
                    motEnCours += "&";
                    labelMot.Text += " _";
                }
            }
            else
            {
                motEnCours += motATrouver[0];
                labelMot.Text += motATrouver[0];
                for (int j = 1; j < motATrouver.Length - 1; j++)
                {
                    motEnCours += "&";
                    labelMot.Text += " _";
                }
                motEnCours += motATrouver[motATrouver.Length - 1];
                labelMot.Text += motATrouver[motATrouver.Length - 1];
            }
            numeroImage = -1;
            numeroImage++;
            rechargerImage();
            pictureBoxPendu.Image = new Bitmap(cheminImage + "0.png");
            listBoxLettreSaisie.Items.Clear();
            textBoxLettreSaisie.Enabled = true;

        }

        /// <summary>
        /// Méthode pour quitter le jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            musique.controls.stop();
            Form.ActiveForm.Close();
        }

        /// <summary>
        /// Méthode permettant de jouer la partie en cours
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonValider_Click(object sender, EventArgs e)
        {
            lettreSaisie = textBoxLettreSaisie.Text.ToUpper();
            textBoxLettreSaisie.Text = "";

            //Si on rentre plus d'une lettre
            if (textBoxLettreSaisie.Text.Length > 1)
            {
                MessageBox.Show("Saisir qu'une lettre");
                textBoxLettreSaisie.Text = "";
                textBoxLettreSaisie.Focus();
            }

            // Si la lettre n'a pas déjà été saisie
            if (listBoxLettreSaisie.FindString(lettreSaisie) == ListBox.NoMatches)
            {
                // on l'ajoute a la listeBox et on continue
                listBoxLettreSaisie.Items.Add(lettreSaisie);
            }
            // Sinon on quitte la méthode après avoir afficher un message
            else
            {
                MessageBox.Show("Vous avez déjà saisi cette lettre");
                return;
            }
            // Si la lettre saisie se trouve dans le mot à trouver
            if (motATrouver.Contains(lettreSaisie))
            {

                motCourant = "";
                for (int i = 0; i < motATrouver.Length; i++)
                {
                    // Si la lettre saisie est égale à la lettre courante du motAtrouver on l'ajoute au mot courant
                    if (motATrouver[i] == Convert.ToChar(lettreSaisie))
                    {
                        motCourant += lettreSaisie;
                    }
                    // Sinon on ajoute la lettre du motEnCours
                    else
                    {
                        motCourant += motEnCours[i];
                    }
                }
                motEnCours = motCourant;
                // On update le label qui affiche le mot en cours en remplacant les "&" par des " _"
                labelMot.Text = "";
                labelMot.Text = motEnCours.Replace("&", " _");
            }

            //Si la lettre saisie ne s trouve pas dans la mot à trouver
            else
            {
                numeroImage++;
                rechargerImage();
                if (numeroImage == 12)
                {
                    MessageBox.Show("Tu n'as pas trouvé le bon mot.\nLe mot était: " + motATrouver);
                    textBoxLettreSaisie.Enabled = false;
                    pictureBoxPendu.BackgroundImage = new Bitmap(cheminImage + "0.png");
                    NouveauJeuToolStripMenuItem_Click(null, null);
                }
            }

            // true quand le joueur a trouver le mot
            if (motEnCours == motATrouver)
            {
                MessageBox.Show("Tu as trouvé le bon mot!!!");
                NouveauJeuToolStripMenuItem_Click(null, null);
            }
        }

        /// <summary>
        /// Afficher les images du pendu
        /// </summary>
        private void rechargerImage()
        {
            if (File.Exists(cheminImage + numeroImage + ".png"))
            {
                pictureBoxPendu = new PictureBox();
                pictureBoxPendu.Image = new Bitmap(cheminImage + numeroImage + ".png");
            }

        }

        /// <summary>
        /// Arreter la musique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBoxMusiqueStop_Click(object sender, EventArgs e)
        {
            musique.controls.stop();
        }

        /// <summary>
        /// Redemarrer la musique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBoxJouerMusique_Click(object sender, EventArgs e)
        {
            musique.controls.play();
        }

        /// <summary>
        /// Arreter la musique lorsque l'on ferme le formulaire 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormPenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            musique.controls.stop();
            MdiChild = new Jeux.vues.FormFenetrePrincipale();
            MdiChild.Show();
            this.Hide();
        }

        public static void SetPseudo(string pseudo)
        {
            utilisateur = pseudo;
        }
    }
}
