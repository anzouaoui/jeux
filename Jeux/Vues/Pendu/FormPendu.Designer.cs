﻿namespace Jeux.Vues.Pendu
{
    partial class FormPendu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelUtilisateur = new System.Windows.Forms.Label();
            this.pictureBoxJouerMusique = new System.Windows.Forms.PictureBox();
            this.pictureBoxMusiqueStop = new System.Windows.Forms.PictureBox();
            this.buttonValider = new System.Windows.Forms.Button();
            this.textBoxLettreSaisie = new System.Windows.Forms.TextBox();
            this.labelSaisieLettre = new System.Windows.Forms.Label();
            this.pictureBoxPendu = new System.Windows.Forms.PictureBox();
            this.listBoxLettreSaisie = new System.Windows.Forms.ListBox();
            this.labelLettreSaisie = new System.Windows.Forms.Label();
            this.aProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aIDEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NouveauJeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fICHIERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelMotATrouver = new System.Windows.Forms.Label();
            this.menuPendu = new System.Windows.Forms.MenuStrip();
            this.labelPendu = new System.Windows.Forms.Label();
            this.labelMot = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxJouerMusique)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMusiqueStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPendu)).BeginInit();
            this.menuPendu.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelUtilisateur
            // 
            this.labelUtilisateur.AutoSize = true;
            this.labelUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUtilisateur.Location = new System.Drawing.Point(756, 24);
            this.labelUtilisateur.Name = "labelUtilisateur";
            this.labelUtilisateur.Size = new System.Drawing.Size(160, 37);
            this.labelUtilisateur.TabIndex = 40;
            this.labelUtilisateur.Text = "utilisateur";
            this.labelUtilisateur.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBoxJouerMusique
            // 
            this.pictureBoxJouerMusique.Location = new System.Drawing.Point(114, 601);
            this.pictureBoxJouerMusique.Name = "pictureBoxJouerMusique";
            this.pictureBoxJouerMusique.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxJouerMusique.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxJouerMusique.TabIndex = 39;
            this.pictureBoxJouerMusique.TabStop = false;
            this.pictureBoxJouerMusique.Click += new System.EventHandler(this.PictureBoxJouerMusique_Click);
            // 
            // pictureBoxMusiqueStop
            // 
            this.pictureBoxMusiqueStop.Location = new System.Drawing.Point(12, 601);
            this.pictureBoxMusiqueStop.Name = "pictureBoxMusiqueStop";
            this.pictureBoxMusiqueStop.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxMusiqueStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxMusiqueStop.TabIndex = 38;
            this.pictureBoxMusiqueStop.TabStop = false;
            this.pictureBoxMusiqueStop.Click += new System.EventHandler(this.PictureBoxMusiqueStop_Click);
            // 
            // buttonValider
            // 
            this.buttonValider.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonValider.Location = new System.Drawing.Point(748, 198);
            this.buttonValider.Name = "buttonValider";
            this.buttonValider.Size = new System.Drawing.Size(121, 31);
            this.buttonValider.TabIndex = 37;
            this.buttonValider.Text = "VALIDER";
            this.buttonValider.UseVisualStyleBackColor = true;
            this.buttonValider.Click += new System.EventHandler(this.ButtonValider_Click);
            // 
            // textBoxLettreSaisie
            // 
            this.textBoxLettreSaisie.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLettreSaisie.Location = new System.Drawing.Point(615, 198);
            this.textBoxLettreSaisie.Name = "textBoxLettreSaisie";
            this.textBoxLettreSaisie.Size = new System.Drawing.Size(51, 31);
            this.textBoxLettreSaisie.TabIndex = 36;
            // 
            // labelSaisieLettre
            // 
            this.labelSaisieLettre.AutoSize = true;
            this.labelSaisieLettre.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSaisieLettre.Location = new System.Drawing.Point(576, 151);
            this.labelSaisieLettre.Name = "labelSaisieLettre";
            this.labelSaisieLettre.Size = new System.Drawing.Size(293, 44);
            this.labelSaisieLettre.TabIndex = 35;
            this.labelSaisieLettre.Text = "Saisir une lettre:";
            // 
            // pictureBoxPendu
            // 
            this.pictureBoxPendu.Location = new System.Drawing.Point(12, 151);
            this.pictureBoxPendu.Name = "pictureBoxPendu";
            this.pictureBoxPendu.Size = new System.Drawing.Size(405, 434);
            this.pictureBoxPendu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPendu.TabIndex = 34;
            this.pictureBoxPendu.TabStop = false;
            // 
            // listBoxLettreSaisie
            // 
            this.listBoxLettreSaisie.FormattingEnabled = true;
            this.listBoxLettreSaisie.Location = new System.Drawing.Point(584, 474);
            this.listBoxLettreSaisie.Name = "listBoxLettreSaisie";
            this.listBoxLettreSaisie.Size = new System.Drawing.Size(279, 173);
            this.listBoxLettreSaisie.TabIndex = 33;
            // 
            // labelLettreSaisie
            // 
            this.labelLettreSaisie.AutoSize = true;
            this.labelLettreSaisie.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLettreSaisie.Location = new System.Drawing.Point(589, 427);
            this.labelLettreSaisie.Name = "labelLettreSaisie";
            this.labelLettreSaisie.Size = new System.Drawing.Size(258, 44);
            this.labelLettreSaisie.TabIndex = 32;
            this.labelLettreSaisie.Text = "Lettres saisies:";
            // 
            // aProposToolStripMenuItem
            // 
            this.aProposToolStripMenuItem.Name = "aProposToolStripMenuItem";
            this.aProposToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aProposToolStripMenuItem.Text = "A propos";
            this.aProposToolStripMenuItem.Click += new System.EventHandler(this.AProposToolStripMenuItem_Click);
            // 
            // aIDEToolStripMenuItem
            // 
            this.aIDEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aProposToolStripMenuItem});
            this.aIDEToolStripMenuItem.Name = "aIDEToolStripMenuItem";
            this.aIDEToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.aIDEToolStripMenuItem.Text = "AIDE";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.QuitterToolStripMenuItem_Click);
            // 
            // NouveauJeuToolStripMenuItem
            // 
            this.NouveauJeuToolStripMenuItem.Name = "NouveauJeuToolStripMenuItem";
            this.NouveauJeuToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.NouveauJeuToolStripMenuItem.Text = "Nouveau jeu";
            this.NouveauJeuToolStripMenuItem.Click += new System.EventHandler(this.NouveauJeuToolStripMenuItem_Click);
            // 
            // fICHIERToolStripMenuItem
            // 
            this.fICHIERToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NouveauJeuToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.fICHIERToolStripMenuItem.Name = "fICHIERToolStripMenuItem";
            this.fICHIERToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.fICHIERToolStripMenuItem.Text = "FICHIER";
            // 
            // labelMotATrouver
            // 
            this.labelMotATrouver.AutoSize = true;
            this.labelMotATrouver.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotATrouver.Location = new System.Drawing.Point(589, 273);
            this.labelMotATrouver.Name = "labelMotATrouver";
            this.labelMotATrouver.Size = new System.Drawing.Size(259, 44);
            this.labelMotATrouver.TabIndex = 30;
            this.labelMotATrouver.Text = "Mot à trouver:";
            // 
            // menuPendu
            // 
            this.menuPendu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fICHIERToolStripMenuItem,
            this.aIDEToolStripMenuItem});
            this.menuPendu.Location = new System.Drawing.Point(0, 0);
            this.menuPendu.Name = "menuPendu";
            this.menuPendu.Size = new System.Drawing.Size(916, 24);
            this.menuPendu.TabIndex = 29;
            this.menuPendu.Text = "menuStrip1";
            // 
            // labelPendu
            // 
            this.labelPendu.AutoSize = true;
            this.labelPendu.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPendu.Location = new System.Drawing.Point(12, 38);
            this.labelPendu.Name = "labelPendu";
            this.labelPendu.Size = new System.Drawing.Size(384, 119);
            this.labelPendu.TabIndex = 28;
            this.labelPendu.Text = "PENDU";
            // 
            // labelMot
            // 
            this.labelMot.AutoSize = true;
            this.labelMot.BackColor = System.Drawing.Color.LightSkyBlue;
            this.labelMot.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMot.Location = new System.Drawing.Point(617, 317);
            this.labelMot.MinimumSize = new System.Drawing.Size(200, 26);
            this.labelMot.Name = "labelMot";
            this.labelMot.Size = new System.Drawing.Size(200, 38);
            this.labelMot.TabIndex = 41;
            this.labelMot.UseMnemonic = false;
            // 
            // FormPendu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(916, 687);
            this.Controls.Add(this.labelMot);
            this.Controls.Add(this.labelUtilisateur);
            this.Controls.Add(this.pictureBoxJouerMusique);
            this.Controls.Add(this.pictureBoxMusiqueStop);
            this.Controls.Add(this.buttonValider);
            this.Controls.Add(this.textBoxLettreSaisie);
            this.Controls.Add(this.labelSaisieLettre);
            this.Controls.Add(this.pictureBoxPendu);
            this.Controls.Add(this.listBoxLettreSaisie);
            this.Controls.Add(this.labelLettreSaisie);
            this.Controls.Add(this.labelMotATrouver);
            this.Controls.Add(this.menuPendu);
            this.Controls.Add(this.labelPendu);
            this.Name = "FormPendu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pendu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPenu_FormClosed);
            this.Load += new System.EventHandler(this.FormPendu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxJouerMusique)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMusiqueStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPendu)).EndInit();
            this.menuPendu.ResumeLayout(false);
            this.menuPendu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelUtilisateur;
        private System.Windows.Forms.PictureBox pictureBoxJouerMusique;
        private System.Windows.Forms.PictureBox pictureBoxMusiqueStop;
        private System.Windows.Forms.Button buttonValider;
        private System.Windows.Forms.TextBox textBoxLettreSaisie;
        private System.Windows.Forms.Label labelSaisieLettre;
        private System.Windows.Forms.PictureBox pictureBoxPendu;
        private System.Windows.Forms.ListBox listBoxLettreSaisie;
        private System.Windows.Forms.Label labelLettreSaisie;
        private System.Windows.Forms.ToolStripMenuItem aProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aIDEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NouveauJeuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fICHIERToolStripMenuItem;
        private System.Windows.Forms.Label labelMotATrouver;
        private System.Windows.Forms.MenuStrip menuPendu;
        private System.Windows.Forms.Label labelPendu;
        private System.Windows.Forms.Label labelMot;
    }
}