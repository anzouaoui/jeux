﻿namespace Jeux.Vues
{
    partial class FormModifierUtilisateur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxMotDePasseUtilisateur = new System.Windows.Forms.TextBox();
            this.textBoxPseudoUtilisateur = new System.Windows.Forms.TextBox();
            this.textBoxNomUtilisateur = new System.Windows.Forms.TextBox();
            this.buttonModifier = new System.Windows.Forms.Button();
            this.labelMotDePasseUtilisateur = new System.Windows.Forms.Label();
            this.labelPseudoUtilisateur = new System.Windows.Forms.Label();
            this.labelNomUtilisateur = new System.Windows.Forms.Label();
            this.labelTitre = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxConfMotDePasse = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxMotDePasseUtilisateur
            // 
            this.textBoxMotDePasseUtilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMotDePasseUtilisateur.Location = new System.Drawing.Point(417, 484);
            this.textBoxMotDePasseUtilisateur.Name = "textBoxMotDePasseUtilisateur";
            this.textBoxMotDePasseUtilisateur.Size = new System.Drawing.Size(346, 40);
            this.textBoxMotDePasseUtilisateur.TabIndex = 36;
            this.textBoxMotDePasseUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxPseudoUtilisateur
            // 
            this.textBoxPseudoUtilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPseudoUtilisateur.Location = new System.Drawing.Point(417, 374);
            this.textBoxPseudoUtilisateur.Name = "textBoxPseudoUtilisateur";
            this.textBoxPseudoUtilisateur.Size = new System.Drawing.Size(346, 40);
            this.textBoxPseudoUtilisateur.TabIndex = 35;
            this.textBoxPseudoUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxNomUtilisateur
            // 
            this.textBoxNomUtilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNomUtilisateur.Location = new System.Drawing.Point(417, 265);
            this.textBoxNomUtilisateur.Name = "textBoxNomUtilisateur";
            this.textBoxNomUtilisateur.Size = new System.Drawing.Size(346, 40);
            this.textBoxNomUtilisateur.TabIndex = 34;
            this.textBoxNomUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonModifier
            // 
            this.buttonModifier.AutoSize = true;
            this.buttonModifier.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonModifier.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonModifier.Location = new System.Drawing.Point(464, 693);
            this.buttonModifier.Name = "buttonModifier";
            this.buttonModifier.Size = new System.Drawing.Size(234, 53);
            this.buttonModifier.TabIndex = 33;
            this.buttonModifier.Text = "MODIFIER";
            this.buttonModifier.UseVisualStyleBackColor = false;
            this.buttonModifier.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // labelMotDePasseUtilisateur
            // 
            this.labelMotDePasseUtilisateur.AutoSize = true;
            this.labelMotDePasseUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotDePasseUtilisateur.Location = new System.Drawing.Point(409, 437);
            this.labelMotDePasseUtilisateur.Name = "labelMotDePasseUtilisateur";
            this.labelMotDePasseUtilisateur.Size = new System.Drawing.Size(277, 44);
            this.labelMotDePasseUtilisateur.TabIndex = 32;
            this.labelMotDePasseUtilisateur.Text = "MOT DE PASSE:";
            // 
            // labelPseudoUtilisateur
            // 
            this.labelPseudoUtilisateur.AutoSize = true;
            this.labelPseudoUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPseudoUtilisateur.Location = new System.Drawing.Point(409, 327);
            this.labelPseudoUtilisateur.Name = "labelPseudoUtilisateur";
            this.labelPseudoUtilisateur.Size = new System.Drawing.Size(170, 44);
            this.labelPseudoUtilisateur.TabIndex = 31;
            this.labelPseudoUtilisateur.Text = "PSEUDO:";
            // 
            // labelNomUtilisateur
            // 
            this.labelNomUtilisateur.AutoSize = true;
            this.labelNomUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomUtilisateur.Location = new System.Drawing.Point(409, 218);
            this.labelNomUtilisateur.Name = "labelNomUtilisateur";
            this.labelNomUtilisateur.Size = new System.Drawing.Size(115, 44);
            this.labelNomUtilisateur.TabIndex = 30;
            this.labelNomUtilisateur.Text = "NOM:";
            // 
            // labelTitre
            // 
            this.labelTitre.AutoSize = true;
            this.labelTitre.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitre.Location = new System.Drawing.Point(186, 49);
            this.labelTitre.Name = "labelTitre";
            this.labelTitre.Size = new System.Drawing.Size(695, 119);
            this.labelTitre.TabIndex = 29;
            this.labelTitre.Text = "MON COMPTE";
            this.labelTitre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(409, 557);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(554, 44);
            this.label1.TabIndex = 37;
            this.label1.Text = "CONFIRME TON MOT DE PASSE:";
            // 
            // textBoxConfMotDePasse
            // 
            this.textBoxConfMotDePasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxConfMotDePasse.Location = new System.Drawing.Point(417, 604);
            this.textBoxConfMotDePasse.Name = "textBoxConfMotDePasse";
            this.textBoxConfMotDePasse.Size = new System.Drawing.Size(346, 40);
            this.textBoxConfMotDePasse.TabIndex = 38;
            this.textBoxConfMotDePasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FormModifierUtilisateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1154, 758);
            this.Controls.Add(this.textBoxConfMotDePasse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxMotDePasseUtilisateur);
            this.Controls.Add(this.textBoxPseudoUtilisateur);
            this.Controls.Add(this.textBoxNomUtilisateur);
            this.Controls.Add(this.buttonModifier);
            this.Controls.Add(this.labelMotDePasseUtilisateur);
            this.Controls.Add(this.labelPseudoUtilisateur);
            this.Controls.Add(this.labelNomUtilisateur);
            this.Controls.Add(this.labelTitre);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormModifierUtilisateur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormModifierUtilisateur";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormModifierUtilisateur_FormClosed);
            this.Load += new System.EventHandler(this.FormModifierUtilisateur_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxMotDePasseUtilisateur;
        private System.Windows.Forms.TextBox textBoxPseudoUtilisateur;
        private System.Windows.Forms.TextBox textBoxNomUtilisateur;
        private System.Windows.Forms.Button buttonModifier;
        private System.Windows.Forms.Label labelMotDePasseUtilisateur;
        private System.Windows.Forms.Label labelPseudoUtilisateur;
        private System.Windows.Forms.Label labelNomUtilisateur;
        private System.Windows.Forms.Label labelTitre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxConfMotDePasse;
    }
}