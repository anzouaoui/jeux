﻿namespace Jeux.Vues.Morpion
{
    partial class FormMenuMorpion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonMorpionOrdinateur = new System.Windows.Forms.Button();
            this.buttonMorpion2Joueur = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonMorpionOrdinateur
            // 
            this.buttonMorpionOrdinateur.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonMorpionOrdinateur.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMorpionOrdinateur.Location = new System.Drawing.Point(133, 231);
            this.buttonMorpionOrdinateur.Name = "buttonMorpionOrdinateur";
            this.buttonMorpionOrdinateur.Size = new System.Drawing.Size(377, 56);
            this.buttonMorpionOrdinateur.TabIndex = 5;
            this.buttonMorpionOrdinateur.Text = "ORDINATEUR";
            this.buttonMorpionOrdinateur.UseVisualStyleBackColor = false;
            this.buttonMorpionOrdinateur.Click += new System.EventHandler(this.buttonMorpionOrdinateur_Click);
            // 
            // buttonMorpion2Joueur
            // 
            this.buttonMorpion2Joueur.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonMorpion2Joueur.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMorpion2Joueur.Location = new System.Drawing.Point(133, 108);
            this.buttonMorpion2Joueur.Name = "buttonMorpion2Joueur";
            this.buttonMorpion2Joueur.Size = new System.Drawing.Size(377, 56);
            this.buttonMorpion2Joueur.TabIndex = 4;
            this.buttonMorpion2Joueur.Text = "2 JOUEURS";
            this.buttonMorpion2Joueur.UseVisualStyleBackColor = false;
            this.buttonMorpion2Joueur.Click += new System.EventHandler(this.ButtonMorpion2Joueur_Click);
            // 
            // FormMenuMorpion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(643, 395);
            this.Controls.Add(this.buttonMorpionOrdinateur);
            this.Controls.Add(this.buttonMorpion2Joueur);
            this.MinimizeBox = false;
            this.Name = "FormMenuMorpion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMenuMorpion";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMenuMorpion_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonMorpionOrdinateur;
        private System.Windows.Forms.Button buttonMorpion2Joueur;
    }
}