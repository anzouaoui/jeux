﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace Jeux.Vues.Morpion.MorpionAi
{
    public partial class FormMorpionAi : Form
    {
        #region VARIBLES
        /// <summary>
        /// Savoir si on a quitter le jeu ou pas
        /// </summary>
        bool quitter = true;

        /// <summary>
        /// Concerne la musique à lancer
        /// </summary>
        WindowsMediaPlayer musique = new WindowsMediaPlayer();

        /// <summary>
        /// objet de type bool
        /// vrai = X, faux = O
        /// </summary>
        bool tour = true;

        /// <summary>
        /// Objet de type entier
        /// Compte le nombre de tour
        /// </summary>
        int CompteurTour = 0;

        /// <summary>
        /// Concerne le bouton sur lequel on appuie
        /// </summary>
        Button boutonSelectionne;

        /// <summary>
        /// Score du joueur connecté
        /// </summary>
        int score = 0;

        /// <summary>
        /// Nom du joueur 1
        /// </summary>
        static string pseudoJoueur;

        /// <summary>
        /// Nom du joueur 2
        /// </summary>
        static string nomJoueur2;

        /// <summary>
        /// Tour de l'ordinateur
        /// </summary>
        bool againstComputer = false;

        Form Mdichild;
        #endregion

        public FormMorpionAi()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Afficher un message de A propos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Par Anouk ZOUAOUI\nBTS SIO SLAM\n lycee Jules Ferry\nMusique: Conmptine d'un autre été, interprétée par Anouk ZOUAOUI, TIERSEN", "A propos du morpion");
        }

        /// <summary>
        /// Quitter l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quitter = false;
            musique.controls.stop();
            Application.Exit();
        }

        /// <summary>
        /// Evenement click d'un bouton pour jouer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, EventArgs e)
        {

            boutonSelectionne = sender as Button;
            if (tour)
            {
                boutonSelectionne.Text = "X";
                boutonSelectionne.BackColor = System.Drawing.Color.Azure;
            }
            else
            {
                boutonSelectionne.Text = "O";
                boutonSelectionne.BackColor = System.Drawing.Color.AntiqueWhite;
            }

            tour = !tour;
            boutonSelectionne.Enabled = false;
            CompteurTour++;
            Gagner();

            #region L'ordinateur qui joue
            if ((!tour) && (againstComputer))
            {
                MouvementOrdinateur();
            }
            #endregion
        }

        // Verifier qui a gagné
        /// </summary>
        /// <returns> retour un booléen gagner</returns>
        private bool Gagner()
        {
            bool gagner = false;

            #region HORIZONTAL
            if ((buttonA1.Text == buttonA2.Text) && (buttonA2.Text == buttonA3.Text) && (!buttonA1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonB1.Text == buttonB2.Text) && (buttonB2.Text == buttonB3.Text) && (!buttonB1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonC1.Text == buttonC2.Text) && (buttonC2.Text == buttonC3.Text) && (!buttonC1.Enabled))
            {
                gagner = true;
            }
            #endregion

            #region VERTICAL
            if ((buttonA1.Text == buttonB1.Text) && (buttonB1.Text == buttonC1.Text) && (!buttonA1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonA2.Text == buttonB2.Text) && (buttonB2.Text == buttonC2.Text) && (!buttonA2.Enabled))
            {
                gagner = true;
            }
            else if ((buttonA3.Text == buttonB3.Text) && (buttonB3.Text == buttonC3.Text) && (!buttonA3.Enabled))
            {
                gagner = true;
            }
            #endregion

            #region DIAGONAL
            if ((buttonA1.Text == buttonB2.Text) && (buttonB2.Text == buttonC3.Text) && (!buttonA1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonA3.Text == buttonB2.Text) && (buttonB2.Text == buttonC1.Text) && (!buttonA3.Enabled))
            {
                gagner = true;
            }
            #endregion

            if (gagner)
            {

                string gagnant = "";
                if (tour)
                {
                    labelNbOGagnant.Text = (Int32.Parse(labelNbOGagnant.Text) + 1).ToString();
                    gagnant = nomJoueur2;
                    score++;
                    MessageBox.Show(gagnant + " a gagne!!\nScore: " + score);

                }
                else
                {

                    labelNbXGagnant.Text = (Int32.Parse(labelNbXGagnant.Text) + 1).ToString();
                    gagnant = pseudoJoueur;
                    MessageBox.Show(gagnant + " a gagne!!");
                }
                arretJeu();

                NouveauJeuToolStripMenuItem_Click(null, null);
            }
            else
            {
                if (CompteurTour == 9)
                {
                    labelNbEgalite.Text = (Int32.Parse(labelNbEgalite.Text) + 1).ToString();
                    if (labelNbEgalite.Text == "5")
                    {
                        MessageBox.Show("Vous avez 5 égalité.\n Le jeu s'arrete");

                    }
                    arretJeu();
                    NouveauJeuToolStripMenuItem_Click(null, null);

                }
            }

            return gagner;
        }

        /// <summary>
        /// Arrete le jeu lorsue quelqu'un à gagné
        /// </summary>
        private void arretJeu()
        {
            try
            {
                foreach (Control controlCourant in Controls)
                {
                    boutonSelectionne = controlCourant as Button;
                    boutonSelectionne.Enabled = false;
                }
            }
            catch { }
        }

        /// <summary>
        /// Redémarer un nouveau jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NouveauJeuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tour = true;
            CompteurTour = 0;

            if (labelNbOGagnant.Text == "3" || labelNbXGagnant.Text == "3" || labelNbEgalite.Text == "5")
            {
                labelNbEgalite.Text = "0";
                labelNbOGagnant.Text = "0";
                labelNbXGagnant.Text = "0";
            }
            foreach (Control controlCourant in Controls)
            {
                boutonSelectionne = controlCourant as Button;
                if (boutonSelectionne != null)
                {
                    boutonSelectionne.Enabled = true;
                    boutonSelectionne.Text = "";
                    if (boutonSelectionne != buttonPlayMusique && boutonSelectionne != buttonStopMusique)
                    {
                        boutonSelectionne.BackColor = Color.Transparent;
                    }
                }

            }

        }

        /// <summary>
        /// Changer le nom du deuxieme joueur en ordinateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelOGagnant_TextChanged(object sender, EventArgs e)
        {
            if (labelOGagnant.Text.ToUpper() == "ORDINATEUR")
            {
                againstComputer = true;
            }
            else
            {
                againstComputer = false;
            }
        }

        /// <summary>
        /// Changer le nom des joueurs
        /// </summary>
        /// <param name="pseudo"></param>
        public static void SetPseudo(string pseudo)
        {
            pseudoJoueur = pseudo;
            nomJoueur2 = "ORDINATEUR";
        }

        /// <summary>
        /// Chargement du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMorpionAi_Load(object sender, EventArgs e)
        {
            labelOGagnant.Text = nomJoueur2;
            labelXGagnant.Text = pseudoJoueur;
        }

        /// <summary>
        /// L'ordinateur joue
        /// </summary>
        private void MouvementOrdinateur()
        {
            Button mouvement = null;

            mouvement = GagnerOuBloquer("O");
            if (mouvement == null)
            {
                mouvement = GagnerOuBloquer("X");
                if (mouvement == null)
                {
                    mouvement = Coin();
                    if (mouvement == null)
                    {
                        mouvement = EspaceVide();
                    }
                }
            }
            mouvement.PerformClick();
        }

        /// <summary>
        /// L'ordiateur gagne ou bloque le joueur
        /// </summary>
        /// <param name="marque"></param>
        /// <returns></returns>
        private Button GagnerOuBloquer(string marque)
        {
            Button unBouton = null;
            #region test horizontaux
            #region Ligne A
            if ((buttonA1.Text == marque) && (buttonA2.Text == marque) && (buttonA3.Text == ""))
            {
                unBouton = buttonA3;
            }
            if ((buttonA2.Text == marque) && (buttonA3.Text == marque) && (buttonA1.Text == ""))
            {
                unBouton = buttonA1;
            }
            if ((buttonA1.Text == marque) && (buttonA3.Text == marque) && (buttonA2.Text == ""))
            {
                unBouton = buttonA2;
            }
            #endregion

            #region Ligne B
            if ((buttonB1.Text == marque) && (buttonB2.Text == marque) && (buttonB3.Text == ""))
            {
                unBouton = buttonB3;
            }
            if ((buttonB2.Text == marque) && (buttonB3.Text == marque) && (buttonB1.Text == ""))
            {
                unBouton = buttonB1;
            }
            if ((buttonB1.Text == marque) && (buttonB3.Text == marque) && (buttonB2.Text == ""))
            {
                unBouton = buttonB2;
            }
            #endregion

            #region Ligne C
            if ((buttonC1.Text == marque) && (buttonC2.Text == marque) && (buttonC3.Text == ""))
            {
                unBouton = buttonC3;
            }
            if ((buttonC2.Text == marque) && (buttonC3.Text == marque) && (buttonC1.Text == ""))
            {
                unBouton = buttonC1;
            }
            if ((buttonC1.Text == marque) && (buttonC3.Text == marque) && (buttonC2.Text == ""))
            {
                unBouton = buttonC2;
            }
            #endregion
            #endregion

            #region test verticaux
            #region Colonne 1
            if ((buttonA1.Text == marque) && (buttonB1.Text == marque) && (buttonC1.Text == ""))
            {
                unBouton = buttonC1;
            }
            if ((buttonB1.Text == marque) && (buttonC1.Text == marque) && (buttonA1.Text == ""))
            {
                unBouton = buttonA1;
            }
            if ((buttonA1.Text == marque) && (buttonC1.Text == marque) && (buttonB1.Text == ""))
            {
                unBouton = buttonB1;
            }
            #endregion

            #region Colonne 2
            if ((buttonA2.Text == marque) && (buttonB2.Text == marque) && (buttonC2.Text == ""))
            {
                unBouton = buttonC2;
            }
            if ((buttonB2.Text == marque) && (buttonC2.Text == marque) && (buttonA2.Text == ""))
            {
                unBouton = buttonA2;
            }
            if ((buttonA2.Text == marque) && (buttonC2.Text == marque) && (buttonB2.Text == ""))
            {
                unBouton = buttonB2;
            }
            #endregion

            #region Colonne 3
            if ((buttonA3.Text == marque) && (buttonB3.Text == marque) && (buttonC3.Text == ""))
            {
                unBouton = buttonC3;
            }
            if ((buttonB3.Text == marque) && (buttonC3.Text == marque) && (buttonA3.Text == ""))
            {
                unBouton = buttonA3;
            }
            if ((buttonA3.Text == marque) && (buttonC3.Text == marque) && (buttonB3.Text == ""))
            {
                unBouton = buttonB3;
            }
            #endregion
            #endregion

            #region test diagonaux
            #region Diagonal 1 
            if ((buttonA1.Text == marque) && (buttonB2.Text == marque) && (buttonC3.Text == ""))
            {
                unBouton = buttonC3;
            }
            if ((buttonA1.Text == marque) && (buttonC3.Text == marque) && (buttonB2.Text == ""))
            {
                unBouton = buttonB2;
            }
            if ((buttonB2.Text == marque) && (buttonC3.Text == marque) && (buttonA1.Text == ""))
            {
                unBouton = buttonA1;
            }
            #endregion

            #region Diagonal 2
            if ((buttonA3.Text == marque) && (buttonB2.Text == marque) && (buttonC1.Text == ""))
            {
                unBouton = buttonC1;
            }
            if ((buttonA1.Text == marque) && (buttonC3.Text == marque) && (buttonB2.Text == ""))
            {
                unBouton = buttonB2;
            }
            if ((buttonB2.Text == marque) && (buttonC1.Text == marque) && (buttonA3.Text == ""))
            {
                unBouton = buttonA3;
            }
            #endregion
            #endregion
            return unBouton;
        }

        /// <summary>
        /// L'ordinateur cherche un coin
        /// </summary>
        /// <returns></returns>
        private Button Coin()
        {
            Button unBouton = null;
            Console.WriteLine("Cherche un coin");
            #region Par rapport au coin A1
            if (buttonA1.Text == "0")
            {
                if (buttonA3.Text == "")
                {
                    unBouton = buttonA3;
                }
                if (buttonC3.Text == "")
                {
                    unBouton = buttonC3;
                }
                if (buttonC1.Text == "")
                {
                    unBouton = buttonC1;
                }
            }
            #endregion

            #region Par rapport au coin A3
            if (buttonA3.Text == "0")
            {
                if (buttonA1.Text == "")
                {
                    unBouton = buttonA1;
                }
                if (buttonC1.Text == "")
                {
                    unBouton = buttonC1;
                }
                if (buttonC3.Text == "")
                {
                    unBouton = buttonC3;
                }
            }
            #endregion

            #region Par rapport au coin C1
            if (buttonC1.Text == "0")
            {
                if (buttonC3.Text == "")
                {
                    unBouton = buttonC3;
                }
                if (buttonA3.Text == "")
                {
                    unBouton = buttonA3;
                }
                if (buttonA1.Text == "")
                {
                    unBouton = buttonA1;
                }
            }
            #endregion

            #region Par rapport au coin C3
            if (buttonC3.Text == "0")
            {
                if (buttonC1.Text == "")
                {
                    unBouton = buttonC1;
                }
                if (buttonA1.Text == "")
                {
                    unBouton = buttonA1;
                }
                if (buttonA3.Text == "")
                {
                    unBouton = buttonA3;
                }
            }
            #endregion
            return unBouton;
        }

        /// <summary>
        /// L'ordinateur recherche un espace vide
        /// </summary>
        /// <returns></returns>
        private Button EspaceVide()
        {
            Console.WriteLine("Recherche d'un espace vide");
            Button unBouton = null;
            
            foreach (Control controlCourant in Controls)
            {
                unBouton = controlCourant as Button;
                if ((unBouton != null) && (unBouton != buttonStopMusique) && (unBouton != buttonPlayMusique))
                {
                    if (unBouton.Text == "")
                    {
                        return unBouton;
                    }
                }
            }
            return null;
        }

        private void FormMorpionAi_FormClosed(object sender, FormClosedEventArgs e)
        {
            Mdichild = new FormMenuMorpion();
            Mdichild.Show();
            this.Hide();
        }
    }
}
