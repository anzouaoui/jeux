﻿namespace Jeux.Vues.Morpion.MorpionAi
{
    partial class FormMorpionAi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPlayMusique = new System.Windows.Forms.Button();
            this.labelNbXGagnant = new System.Windows.Forms.Label();
            this.buttonStopMusique = new System.Windows.Forms.Button();
            this.panelEgalite = new System.Windows.Forms.Panel();
            this.labelNbEgalite = new System.Windows.Forms.Label();
            this.labelEgalite = new System.Windows.Forms.Label();
            this.labelXGagnant = new System.Windows.Forms.Label();
            this.panelOgagnant = new System.Windows.Forms.Panel();
            this.labelNbOGagnant = new System.Windows.Forms.Label();
            this.labelOGagnant = new System.Windows.Forms.Label();
            this.panelXGagnant = new System.Windows.Forms.Panel();
            this.buttonC3 = new System.Windows.Forms.Button();
            this.buttonB3 = new System.Windows.Forms.Button();
            this.buttonB2 = new System.Windows.Forms.Button();
            this.buttonB1 = new System.Windows.Forms.Button();
            this.buttonA2 = new System.Windows.Forms.Button();
            this.buttonC2 = new System.Windows.Forms.Button();
            this.buttonA3 = new System.Windows.Forms.Button();
            this.buttonA1 = new System.Windows.Forms.Button();
            this.labelMorpion = new System.Windows.Forms.Label();
            this.aProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aIDEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NouveauJeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fICHIERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonC1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.panelEgalite.SuspendLayout();
            this.panelOgagnant.SuspendLayout();
            this.panelXGagnant.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPlayMusique
            // 
            this.buttonPlayMusique.AutoSize = true;
            this.buttonPlayMusique.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonPlayMusique.Location = new System.Drawing.Point(761, 632);
            this.buttonPlayMusique.Name = "buttonPlayMusique";
            this.buttonPlayMusique.Size = new System.Drawing.Size(70, 70);
            this.buttonPlayMusique.TabIndex = 81;
            this.buttonPlayMusique.UseVisualStyleBackColor = false;
            // 
            // labelNbXGagnant
            // 
            this.labelNbXGagnant.AutoSize = true;
            this.labelNbXGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbXGagnant.Location = new System.Drawing.Point(66, 53);
            this.labelNbXGagnant.Name = "labelNbXGagnant";
            this.labelNbXGagnant.Size = new System.Drawing.Size(36, 37);
            this.labelNbXGagnant.TabIndex = 33;
            this.labelNbXGagnant.Text = "0";
            this.labelNbXGagnant.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // buttonStopMusique
            // 
            this.buttonStopMusique.AutoSize = true;
            this.buttonStopMusique.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonStopMusique.Location = new System.Drawing.Point(12, 632);
            this.buttonStopMusique.Name = "buttonStopMusique";
            this.buttonStopMusique.Size = new System.Drawing.Size(75, 70);
            this.buttonStopMusique.TabIndex = 80;
            this.buttonStopMusique.UseVisualStyleBackColor = false;
            // 
            // panelEgalite
            // 
            this.panelEgalite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelEgalite.Controls.Add(this.labelNbEgalite);
            this.panelEgalite.Controls.Add(this.labelEgalite);
            this.panelEgalite.Location = new System.Drawing.Point(334, 499);
            this.panelEgalite.Name = "panelEgalite";
            this.panelEgalite.Size = new System.Drawing.Size(197, 96);
            this.panelEgalite.TabIndex = 78;
            // 
            // labelNbEgalite
            // 
            this.labelNbEgalite.AutoSize = true;
            this.labelNbEgalite.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbEgalite.Location = new System.Drawing.Point(72, 55);
            this.labelNbEgalite.Name = "labelNbEgalite";
            this.labelNbEgalite.Size = new System.Drawing.Size(36, 37);
            this.labelNbEgalite.TabIndex = 34;
            this.labelNbEgalite.Text = "0";
            this.labelNbEgalite.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelEgalite
            // 
            this.labelEgalite.AutoSize = true;
            this.labelEgalite.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEgalite.Location = new System.Drawing.Point(27, 0);
            this.labelEgalite.Name = "labelEgalite";
            this.labelEgalite.Size = new System.Drawing.Size(122, 37);
            this.labelEgalite.TabIndex = 31;
            this.labelEgalite.Text = "égalité:";
            this.labelEgalite.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelXGagnant
            // 
            this.labelXGagnant.AutoSize = true;
            this.labelXGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelXGagnant.Location = new System.Drawing.Point(3, -2);
            this.labelXGagnant.Name = "labelXGagnant";
            this.labelXGagnant.Size = new System.Drawing.Size(172, 37);
            this.labelXGagnant.TabIndex = 30;
            this.labelXGagnant.Text = "X gagnant:";
            this.labelXGagnant.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelOgagnant
            // 
            this.panelOgagnant.AutoSize = true;
            this.panelOgagnant.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelOgagnant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelOgagnant.Controls.Add(this.labelNbOGagnant);
            this.panelOgagnant.Controls.Add(this.labelOGagnant);
            this.panelOgagnant.Location = new System.Drawing.Point(537, 499);
            this.panelOgagnant.Name = "panelOgagnant";
            this.panelOgagnant.Size = new System.Drawing.Size(189, 96);
            this.panelOgagnant.TabIndex = 79;
            // 
            // labelNbOGagnant
            // 
            this.labelNbOGagnant.AutoSize = true;
            this.labelNbOGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbOGagnant.Location = new System.Drawing.Point(72, 55);
            this.labelNbOGagnant.Name = "labelNbOGagnant";
            this.labelNbOGagnant.Size = new System.Drawing.Size(36, 37);
            this.labelNbOGagnant.TabIndex = 34;
            this.labelNbOGagnant.Text = "0";
            this.labelNbOGagnant.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelOGagnant
            // 
            this.labelOGagnant.AutoSize = true;
            this.labelOGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOGagnant.Location = new System.Drawing.Point(6, 0);
            this.labelOGagnant.Name = "labelOGagnant";
            this.labelOGagnant.Size = new System.Drawing.Size(176, 37);
            this.labelOGagnant.TabIndex = 32;
            this.labelOGagnant.Text = "O gagnant:";
            this.labelOGagnant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelOGagnant.TextChanged += new System.EventHandler(this.LabelOGagnant_TextChanged);
            // 
            // panelXGagnant
            // 
            this.panelXGagnant.AutoSize = true;
            this.panelXGagnant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelXGagnant.Controls.Add(this.labelXGagnant);
            this.panelXGagnant.Controls.Add(this.labelNbXGagnant);
            this.panelXGagnant.Location = new System.Drawing.Point(130, 499);
            this.panelXGagnant.Name = "panelXGagnant";
            this.panelXGagnant.Size = new System.Drawing.Size(198, 96);
            this.panelXGagnant.TabIndex = 77;
            // 
            // buttonC3
            // 
            this.buttonC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC3.Location = new System.Drawing.Point(495, 358);
            this.buttonC3.Name = "buttonC3";
            this.buttonC3.Size = new System.Drawing.Size(80, 80);
            this.buttonC3.TabIndex = 76;
            this.buttonC3.UseVisualStyleBackColor = true;
            this.buttonC3.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonB3
            // 
            this.buttonB3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB3.Location = new System.Drawing.Point(495, 253);
            this.buttonB3.Name = "buttonB3";
            this.buttonB3.Size = new System.Drawing.Size(80, 80);
            this.buttonB3.TabIndex = 73;
            this.buttonB3.UseVisualStyleBackColor = true;
            this.buttonB3.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonB2
            // 
            this.buttonB2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB2.Location = new System.Drawing.Point(394, 253);
            this.buttonB2.Name = "buttonB2";
            this.buttonB2.Size = new System.Drawing.Size(80, 80);
            this.buttonB2.TabIndex = 72;
            this.buttonB2.UseVisualStyleBackColor = true;
            this.buttonB2.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonB1
            // 
            this.buttonB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB1.Location = new System.Drawing.Point(299, 253);
            this.buttonB1.Name = "buttonB1";
            this.buttonB1.Size = new System.Drawing.Size(80, 80);
            this.buttonB1.TabIndex = 71;
            this.buttonB1.UseVisualStyleBackColor = true;
            this.buttonB1.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonA2
            // 
            this.buttonA2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA2.Location = new System.Drawing.Point(394, 156);
            this.buttonA2.Name = "buttonA2";
            this.buttonA2.Size = new System.Drawing.Size(80, 80);
            this.buttonA2.TabIndex = 70;
            this.buttonA2.UseVisualStyleBackColor = true;
            this.buttonA2.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonC2
            // 
            this.buttonC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC2.Location = new System.Drawing.Point(394, 358);
            this.buttonC2.Name = "buttonC2";
            this.buttonC2.Size = new System.Drawing.Size(80, 80);
            this.buttonC2.TabIndex = 75;
            this.buttonC2.UseVisualStyleBackColor = true;
            this.buttonC2.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonA3
            // 
            this.buttonA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA3.Location = new System.Drawing.Point(495, 156);
            this.buttonA3.Name = "buttonA3";
            this.buttonA3.Size = new System.Drawing.Size(80, 80);
            this.buttonA3.TabIndex = 69;
            this.buttonA3.UseVisualStyleBackColor = true;
            this.buttonA3.Click += new System.EventHandler(this.Button_Click);
            // 
            // buttonA1
            // 
            this.buttonA1.BackColor = System.Drawing.Color.Transparent;
            this.buttonA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA1.Location = new System.Drawing.Point(299, 156);
            this.buttonA1.Name = "buttonA1";
            this.buttonA1.Size = new System.Drawing.Size(80, 80);
            this.buttonA1.TabIndex = 68;
            this.buttonA1.UseVisualStyleBackColor = false;
            this.buttonA1.Click += new System.EventHandler(this.Button_Click);
            // 
            // labelMorpion
            // 
            this.labelMorpion.AutoSize = true;
            this.labelMorpion.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMorpion.Location = new System.Drawing.Point(169, 24);
            this.labelMorpion.Name = "labelMorpion";
            this.labelMorpion.Size = new System.Drawing.Size(518, 119);
            this.labelMorpion.TabIndex = 67;
            this.labelMorpion.Text = "MORPION";
            // 
            // aProposToolStripMenuItem
            // 
            this.aProposToolStripMenuItem.Name = "aProposToolStripMenuItem";
            this.aProposToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aProposToolStripMenuItem.Text = "A propos";
            this.aProposToolStripMenuItem.Click += new System.EventHandler(this.AProposToolStripMenuItem_Click);
            // 
            // aIDEToolStripMenuItem
            // 
            this.aIDEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aProposToolStripMenuItem});
            this.aIDEToolStripMenuItem.Name = "aIDEToolStripMenuItem";
            this.aIDEToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.aIDEToolStripMenuItem.Text = "AIDE";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.QuitterToolStripMenuItem_Click);
            // 
            // NouveauJeuToolStripMenuItem
            // 
            this.NouveauJeuToolStripMenuItem.Name = "NouveauJeuToolStripMenuItem";
            this.NouveauJeuToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.NouveauJeuToolStripMenuItem.Text = "Nouveau jeu";
            this.NouveauJeuToolStripMenuItem.Click += new System.EventHandler(this.NouveauJeuToolStripMenuItem_Click);
            // 
            // fICHIERToolStripMenuItem
            // 
            this.fICHIERToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NouveauJeuToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.fICHIERToolStripMenuItem.Name = "fICHIERToolStripMenuItem";
            this.fICHIERToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.fICHIERToolStripMenuItem.Text = "FICHIER";
            // 
            // buttonC1
            // 
            this.buttonC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC1.Location = new System.Drawing.Point(299, 358);
            this.buttonC1.Name = "buttonC1";
            this.buttonC1.Size = new System.Drawing.Size(80, 80);
            this.buttonC1.TabIndex = 74;
            this.buttonC1.UseVisualStyleBackColor = true;
            this.buttonC1.Click += new System.EventHandler(this.Button_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fICHIERToolStripMenuItem,
            this.aIDEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(860, 24);
            this.menuStrip1.TabIndex = 66;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // FormMorpionAi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(860, 714);
            this.Controls.Add(this.buttonPlayMusique);
            this.Controls.Add(this.buttonStopMusique);
            this.Controls.Add(this.panelEgalite);
            this.Controls.Add(this.panelOgagnant);
            this.Controls.Add(this.panelXGagnant);
            this.Controls.Add(this.buttonC3);
            this.Controls.Add(this.buttonB3);
            this.Controls.Add(this.buttonB2);
            this.Controls.Add(this.buttonB1);
            this.Controls.Add(this.buttonA2);
            this.Controls.Add(this.buttonC2);
            this.Controls.Add(this.buttonA3);
            this.Controls.Add(this.buttonA1);
            this.Controls.Add(this.labelMorpion);
            this.Controls.Add(this.buttonC1);
            this.Controls.Add(this.menuStrip1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMorpionAi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMorpionAi";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMorpionAi_FormClosed);
            this.Load += new System.EventHandler(this.FormMorpionAi_Load);
            this.panelEgalite.ResumeLayout(false);
            this.panelEgalite.PerformLayout();
            this.panelOgagnant.ResumeLayout(false);
            this.panelOgagnant.PerformLayout();
            this.panelXGagnant.ResumeLayout(false);
            this.panelXGagnant.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPlayMusique;
        private System.Windows.Forms.Label labelNbXGagnant;
        private System.Windows.Forms.Button buttonStopMusique;
        private System.Windows.Forms.Panel panelEgalite;
        private System.Windows.Forms.Label labelNbEgalite;
        private System.Windows.Forms.Label labelEgalite;
        private System.Windows.Forms.Label labelXGagnant;
        private System.Windows.Forms.Panel panelOgagnant;
        private System.Windows.Forms.Label labelNbOGagnant;
        private System.Windows.Forms.Label labelOGagnant;
        private System.Windows.Forms.Panel panelXGagnant;
        private System.Windows.Forms.Button buttonC3;
        private System.Windows.Forms.Button buttonB3;
        private System.Windows.Forms.Button buttonB2;
        private System.Windows.Forms.Button buttonB1;
        private System.Windows.Forms.Button buttonA2;
        private System.Windows.Forms.Button buttonC2;
        private System.Windows.Forms.Button buttonA3;
        private System.Windows.Forms.Button buttonA1;
        private System.Windows.Forms.Label labelMorpion;
        private System.Windows.Forms.ToolStripMenuItem aProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aIDEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NouveauJeuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fICHIERToolStripMenuItem;
        private System.Windows.Forms.Button buttonC1;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}