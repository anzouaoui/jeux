﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeux.Vues.Morpion
{
    public partial class FormMenuMorpion : Form
    {
        Form Mdichild;
        public FormMenuMorpion()
        {
            InitializeComponent();
        }

        private void ButtonMorpion2Joueur_Click(object sender, EventArgs e)
        {
            Mdichild = new Jeux.Vues.Morpion.Morpion2Joueurs.FormMorpion2Joueurs();
            Mdichild.Show();
            this.Hide();
        }

        private void buttonMorpionOrdinateur_Click(object sender, EventArgs e)
        {
            Mdichild = new Jeux.Vues.Morpion.MorpionAi.FormMorpionAi();
            Mdichild.Show();
            this.Hide();
        }

        private void FormMenuMorpion_FormClosed(object sender, FormClosedEventArgs e)
        {
            Mdichild = new Jeux.vues.FormFenetrePrincipale();
            Mdichild.Show();
            this.Hide();
        }
    }
}
