﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeux.Vues.Morpion.Morpion2Joueurs
{
    public partial class FormMorpion2Joueurs : Form
    {
        static string joueurPrincipal;
        Form Mdichild;
        public FormMorpion2Joueurs()
        {
            InitializeComponent();
        }
        public static void SetPseudo(string pseudo)
        {
            joueurPrincipal = pseudo;
        }

        private void FormMorpion2Joueurs_Load(object sender, EventArgs e)
        {
            textBoxNomPremierJoueur.Text = joueurPrincipal;
        }

        private void ButtonJouer_Click(object sender, EventArgs e)
        {
            textBoxNomPremierJoueur.Text = joueurPrincipal;
            
            Jeux.Vues.Morpion.Morpion2Joueurs.FormMorpion2JoueursJeu.SetNomJoueurs(textBoxNomPremierJoueur.Text, textBoxNomDeuxiemeJoueur.Text);
            Mdichild = new Jeux.Vues.Morpion.Morpion2Joueurs.FormMorpion2JoueursJeu();
            Mdichild.Show();
            this.Hide();
        }

        private void TextBoxNomDeuxiemeJoueur_Leave(object sender, EventArgs e)
        {
            if (textBoxNomDeuxiemeJoueur.Text == "")
            {
                MessageBox.Show("Veuillez saisir le nom du deuxième joueur");
                textBoxNomDeuxiemeJoueur.Text = "";
                textBoxNomDeuxiemeJoueur.Focus();
            }
        }

        private void FormMorpion2Joueurs_FormClosed(object sender, FormClosedEventArgs e)
        {
            Mdichild = new Jeux.Vues.Morpion.FormMenuMorpion();
            Mdichild.Show();
            this.Hide();
        }
    }
}
