﻿namespace Jeux.Vues.Morpion.Morpion2Joueurs
{
    partial class FormMorpion2Joueurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonJouer = new System.Windows.Forms.Button();
            this.textBoxNomDeuxiemeJoueur = new System.Windows.Forms.TextBox();
            this.textBoxNomPremierJoueur = new System.Windows.Forms.TextBox();
            this.labelDeuxiemeJoueur = new System.Windows.Forms.Label();
            this.labelPremierJoueur = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonJouer
            // 
            this.buttonJouer.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonJouer.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonJouer.Location = new System.Drawing.Point(519, 286);
            this.buttonJouer.Name = "buttonJouer";
            this.buttonJouer.Size = new System.Drawing.Size(152, 56);
            this.buttonJouer.TabIndex = 20;
            this.buttonJouer.Text = "JOUER";
            this.buttonJouer.UseVisualStyleBackColor = false;
            this.buttonJouer.Click += new System.EventHandler(this.ButtonJouer_Click);
            // 
            // textBoxNomDeuxiemeJoueur
            // 
            this.textBoxNomDeuxiemeJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNomDeuxiemeJoueur.Location = new System.Drawing.Point(390, 178);
            this.textBoxNomDeuxiemeJoueur.Name = "textBoxNomDeuxiemeJoueur";
            this.textBoxNomDeuxiemeJoueur.Size = new System.Drawing.Size(281, 40);
            this.textBoxNomDeuxiemeJoueur.TabIndex = 19;
            this.textBoxNomDeuxiemeJoueur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNomDeuxiemeJoueur.Leave += new System.EventHandler(this.TextBoxNomDeuxiemeJoueur_Leave);
            // 
            // textBoxNomPremierJoueur
            // 
            this.textBoxNomPremierJoueur.Enabled = false;
            this.textBoxNomPremierJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNomPremierJoueur.Location = new System.Drawing.Point(390, 84);
            this.textBoxNomPremierJoueur.Name = "textBoxNomPremierJoueur";
            this.textBoxNomPremierJoueur.Size = new System.Drawing.Size(281, 40);
            this.textBoxNomPremierJoueur.TabIndex = 18;
            this.textBoxNomPremierJoueur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelDeuxiemeJoueur
            // 
            this.labelDeuxiemeJoueur.AutoSize = true;
            this.labelDeuxiemeJoueur.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeuxiemeJoueur.Location = new System.Drawing.Point(23, 178);
            this.labelDeuxiemeJoueur.Name = "labelDeuxiemeJoueur";
            this.labelDeuxiemeJoueur.Size = new System.Drawing.Size(369, 37);
            this.labelDeuxiemeJoueur.TabIndex = 17;
            this.labelDeuxiemeJoueur.Text = "Nom du deuxième joueur:";
            // 
            // labelPremierJoueur
            // 
            this.labelPremierJoueur.AutoSize = true;
            this.labelPremierJoueur.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPremierJoueur.Location = new System.Drawing.Point(23, 84);
            this.labelPremierJoueur.Name = "labelPremierJoueur";
            this.labelPremierJoueur.Size = new System.Drawing.Size(345, 37);
            this.labelPremierJoueur.TabIndex = 16;
            this.labelPremierJoueur.Text = "Nom du premier joueur:";
            // 
            // FormMorpion2Joueurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(814, 410);
            this.Controls.Add(this.buttonJouer);
            this.Controls.Add(this.textBoxNomDeuxiemeJoueur);
            this.Controls.Add(this.textBoxNomPremierJoueur);
            this.Controls.Add(this.labelDeuxiemeJoueur);
            this.Controls.Add(this.labelPremierJoueur);
            this.Name = "FormMorpion2Joueurs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMorpion2Joueurs";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMorpion2Joueurs_FormClosed);
            this.Load += new System.EventHandler(this.FormMorpion2Joueurs_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonJouer;
        private System.Windows.Forms.TextBox textBoxNomDeuxiemeJoueur;
        private System.Windows.Forms.TextBox textBoxNomPremierJoueur;
        private System.Windows.Forms.Label labelDeuxiemeJoueur;
        private System.Windows.Forms.Label labelPremierJoueur;
    }
}