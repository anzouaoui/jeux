﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using Jeux.Properties;

namespace Jeux.Vues.Morpion.Morpion2Joueurs
{
    public partial class FormMorpion2JoueursJeu : Form
    {
        #region VARIABLES
        Form Mdichild;

        /// <summary>
        /// objet de type bool
        /// vrai = X, faux = O
        /// </summary>
        bool tour = true;

        /// <summary>
        /// Savoir si on a quitter le jeu ou pas
        /// </summary>
        bool quitter = true;

        /// <summary>
        /// Objet de type entier
        /// Compte le nombre de tour
        /// </summary>
        int CompteurTour = 0;

        /// <summary>
        /// Score du joueur connecté
        /// </summary>
        int score = 0;

        /// <summary>
        /// Concerne le bouton sur lequel on appuie
        /// </summary>
        Button boutonSelectionne;

        /// <summary>
        /// Concerne la musique à lancer
        /// </summary>
        WindowsMediaPlayer musique = new WindowsMediaPlayer();

        /// <summary>
        /// Nom du joueur 1
        /// </summary>
        static string nomJoueur1;

        /// <summary>
        /// Nom du joueur 2
        /// </summary>
        static string nomJoueur2;
        #endregion

        public FormMorpion2JoueursJeu()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// Enregistrer le jeu 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMorpion2JoueursJeu_Load(object sender, EventArgs e)
        {
            musique.controls.play();
            labelOGagnant.Text = nomJoueur1;
            labelXGagnant.Text = nomJoueur2;

        }

        /// <summary>
        /// Afficher un message de A propos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Par Anouk ZOUAOUI\nBTS SIO SLAM\n lycee Jules Ferry\nMusique: Conmptine d'un autre été, interprétée par Anouk ZOUAOUI, TIERSEN", "A propos du morpion");
        }

        /// <summary>
        /// Quitter l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quitter = false;
            musique.controls.stop();
            Application.Exit();
        }

        /// <summary>
        /// Evenement click d'un bouton pour jouer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, EventArgs e)
        {
            boutonSelectionne = sender as Button;


            if (tour)
            {
                boutonSelectionne.Text = "X";
                boutonSelectionne.BackColor = System.Drawing.Color.Azure;
            }
            else
            {
                boutonSelectionne.Text = "O";
                boutonSelectionne.BackColor = System.Drawing.Color.AntiqueWhite;
            }

            tour = !tour;
            boutonSelectionne.Enabled = false;
            CompteurTour++;
            Gagner();
        }

        /// Verifier qui a gagné
        /// </summary>
        /// <returns> retour un booléen gagner</returns>
        private bool Gagner()
        {
            bool gagner = false;

            #region HORIZONTAL
            if ((buttonA1.Text == buttonA2.Text) && (buttonA2.Text == buttonA3.Text) && (!buttonA1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonB1.Text == buttonB2.Text) && (buttonB2.Text == buttonB3.Text) && (!buttonB1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonC1.Text == buttonC2.Text) && (buttonC2.Text == buttonC3.Text) && (!buttonC1.Enabled))
            {
                gagner = true;
            }
            #endregion

            #region VERTICAL
            if ((buttonA1.Text == buttonB1.Text) && (buttonB1.Text == buttonC1.Text) && (!buttonA1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonA2.Text == buttonB2.Text) && (buttonB2.Text == buttonC2.Text) && (!buttonA2.Enabled))
            {
                gagner = true;
            }
            else if ((buttonA3.Text == buttonB3.Text) && (buttonB3.Text == buttonC3.Text) && (!buttonA3.Enabled))
            {
                gagner = true;
            }
            #endregion

            #region DIAGONAL
            if ((buttonA1.Text == buttonB2.Text) && (buttonB2.Text == buttonC3.Text) && (!buttonA1.Enabled))
            {
                gagner = true;
            }
            else if ((buttonA3.Text == buttonB2.Text) && (buttonB2.Text == buttonC1.Text) && (!buttonA3.Enabled))
            {
                gagner = true;
            }
            #endregion

            if (gagner)
            {

                string gagnant = "";
                if (tour)
                {
                    labelNbOGagnant.Text = (Int32.Parse(labelNbOGagnant.Text) + 1).ToString();
                    gagnant = nomJoueur1;
                    score++;
                    MessageBox.Show(gagnant + " a gagne!!\nScore: " + score);

                }
                else
                {

                    labelNbXGagnant.Text = (Int32.Parse(labelNbXGagnant.Text) + 1).ToString();
                    gagnant = nomJoueur2;
                    MessageBox.Show(gagnant + " a gagne!!");
                }
                arretJeu();

                NouveauJeuToolStripMenuItem_Click(null, null);
            }
            else
            {
                if (CompteurTour == 9)
                {
                    labelNbEgalite.Text = (Int32.Parse(labelNbEgalite.Text) + 1).ToString();
                    if (labelNbEgalite.Text == "5")
                    {
                        MessageBox.Show("Vous avez 5 égalité.\n Le jeu s'arrete");

                    }
                    arretJeu();
                    NouveauJeuToolStripMenuItem_Click(null, null);

                }
            }

            return gagner;
        }

        /// <summary>
        /// Arrete le jeu lorsue quelqu'un à gagné
        /// </summary>
        private void arretJeu()
        {
            try
            {
                foreach (Control controlCourant in Controls)
                {
                    boutonSelectionne = controlCourant as Button;
                    boutonSelectionne.Enabled = false;
                }
            }
            catch { }
        }

        /// <summary>
        /// Redémarer un nouveau jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NouveauJeuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tour = true;
            CompteurTour = 0;

            if (labelNbOGagnant.Text == "3" || labelNbXGagnant.Text == "3" || labelNbEgalite.Text == "5")
            {
                labelNbEgalite.Text = "0";
                labelNbOGagnant.Text = "0";
                labelNbXGagnant.Text = "0";
            }
            foreach (Control controlCourant in Controls)
            {
                boutonSelectionne = controlCourant as Button;
                if (boutonSelectionne != null)
                {
                    boutonSelectionne.Enabled = true;
                    boutonSelectionne.Text = "";
                    if (boutonSelectionne != buttonPlayMusique && boutonSelectionne != buttonStopMusique)
                    {
                        boutonSelectionne.BackColor = Color.Transparent;
                    }
                }

            }

        }

        /// <summary>
        /// Arreter la musique lorsque le formulaire se ferme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMorpion_FormClosed(object sender, FormClosedEventArgs e)
        {

            musique.controls.stop();
            Mdichild = new FormMorpion2Joueurs();
            Mdichild.Show();
            this.Hide();

        }

        /// <summary>
        /// Changer le nom des joueurs
        /// </summary>
        /// <param name="nom1"></param>
        /// <param name="nom2"></param>
        public static void SetNomJoueurs(string nom1, string nom2)
        {
            nomJoueur1 = nom1;
            nomJoueur2 = nom2;
        }

        private void buttonStopMusique_Click(object sender, EventArgs e)
        {

        }

        private void buttonPlayMusique_Click(object sender, EventArgs e)
        {

        }

        private void FormMorpion2JJoueursJeu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Mdichild = new FormMenuMorpion();
            Mdichild.Show();
            this.Hide();
        }
    }
}
