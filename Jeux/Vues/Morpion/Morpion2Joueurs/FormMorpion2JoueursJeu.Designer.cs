﻿namespace Jeux.Vues.Morpion.Morpion2Joueurs
{
    partial class FormMorpion2JoueursJeu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNbOGagnant = new System.Windows.Forms.Label();
            this.panelOgagnant = new System.Windows.Forms.Panel();
            this.labelOGagnant = new System.Windows.Forms.Label();
            this.labelNbEgalite = new System.Windows.Forms.Label();
            this.panelEgalite = new System.Windows.Forms.Panel();
            this.labelEgalite = new System.Windows.Forms.Label();
            this.panelXGagnant = new System.Windows.Forms.Panel();
            this.labelXGagnant = new System.Windows.Forms.Label();
            this.labelNbXGagnant = new System.Windows.Forms.Label();
            this.labelMorpion = new System.Windows.Forms.Label();
            this.buttonC3 = new System.Windows.Forms.Button();
            this.buttonC2 = new System.Windows.Forms.Button();
            this.buttonB3 = new System.Windows.Forms.Button();
            this.buttonB2 = new System.Windows.Forms.Button();
            this.buttonB1 = new System.Windows.Forms.Button();
            this.buttonA2 = new System.Windows.Forms.Button();
            this.buttonA3 = new System.Windows.Forms.Button();
            this.buttonA1 = new System.Windows.Forms.Button();
            this.aProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aIDEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NouveauJeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fICHIERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonC1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buttonPlayMusique = new System.Windows.Forms.Button();
            this.buttonStopMusique = new System.Windows.Forms.Button();
            this.panelOgagnant.SuspendLayout();
            this.panelEgalite.SuspendLayout();
            this.panelXGagnant.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelNbOGagnant
            // 
            this.labelNbOGagnant.AutoSize = true;
            this.labelNbOGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbOGagnant.Location = new System.Drawing.Point(72, 55);
            this.labelNbOGagnant.Name = "labelNbOGagnant";
            this.labelNbOGagnant.Size = new System.Drawing.Size(36, 37);
            this.labelNbOGagnant.TabIndex = 34;
            this.labelNbOGagnant.Text = "0";
            this.labelNbOGagnant.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panelOgagnant
            // 
            this.panelOgagnant.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelOgagnant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelOgagnant.Controls.Add(this.labelNbOGagnant);
            this.panelOgagnant.Controls.Add(this.labelOGagnant);
            this.panelOgagnant.Location = new System.Drawing.Point(549, 495);
            this.panelOgagnant.Name = "panelOgagnant";
            this.panelOgagnant.Size = new System.Drawing.Size(189, 96);
            this.panelOgagnant.TabIndex = 55;
            // 
            // labelOGagnant
            // 
            this.labelOGagnant.AutoSize = true;
            this.labelOGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOGagnant.Location = new System.Drawing.Point(6, 0);
            this.labelOGagnant.Name = "labelOGagnant";
            this.labelOGagnant.Size = new System.Drawing.Size(176, 37);
            this.labelOGagnant.TabIndex = 32;
            this.labelOGagnant.Text = "O gagnant:";
            this.labelOGagnant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNbEgalite
            // 
            this.labelNbEgalite.AutoSize = true;
            this.labelNbEgalite.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbEgalite.Location = new System.Drawing.Point(72, 55);
            this.labelNbEgalite.Name = "labelNbEgalite";
            this.labelNbEgalite.Size = new System.Drawing.Size(36, 37);
            this.labelNbEgalite.TabIndex = 34;
            this.labelNbEgalite.Text = "0";
            this.labelNbEgalite.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panelEgalite
            // 
            this.panelEgalite.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelEgalite.Controls.Add(this.labelNbEgalite);
            this.panelEgalite.Controls.Add(this.labelEgalite);
            this.panelEgalite.Location = new System.Drawing.Point(346, 495);
            this.panelEgalite.Name = "panelEgalite";
            this.panelEgalite.Size = new System.Drawing.Size(197, 96);
            this.panelEgalite.TabIndex = 54;
            // 
            // labelEgalite
            // 
            this.labelEgalite.AutoSize = true;
            this.labelEgalite.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEgalite.Location = new System.Drawing.Point(27, 0);
            this.labelEgalite.Name = "labelEgalite";
            this.labelEgalite.Size = new System.Drawing.Size(122, 37);
            this.labelEgalite.TabIndex = 31;
            this.labelEgalite.Text = "égalité:";
            this.labelEgalite.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelXGagnant
            // 
            this.panelXGagnant.AutoSize = true;
            this.panelXGagnant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelXGagnant.Controls.Add(this.labelXGagnant);
            this.panelXGagnant.Controls.Add(this.labelNbXGagnant);
            this.panelXGagnant.Location = new System.Drawing.Point(142, 495);
            this.panelXGagnant.Name = "panelXGagnant";
            this.panelXGagnant.Size = new System.Drawing.Size(198, 96);
            this.panelXGagnant.TabIndex = 53;
            // 
            // labelXGagnant
            // 
            this.labelXGagnant.AutoSize = true;
            this.labelXGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelXGagnant.Location = new System.Drawing.Point(3, -2);
            this.labelXGagnant.Name = "labelXGagnant";
            this.labelXGagnant.Size = new System.Drawing.Size(172, 37);
            this.labelXGagnant.TabIndex = 30;
            this.labelXGagnant.Text = "X gagnant:";
            this.labelXGagnant.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelNbXGagnant
            // 
            this.labelNbXGagnant.AutoSize = true;
            this.labelNbXGagnant.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbXGagnant.Location = new System.Drawing.Point(66, 53);
            this.labelNbXGagnant.Name = "labelNbXGagnant";
            this.labelNbXGagnant.Size = new System.Drawing.Size(36, 37);
            this.labelNbXGagnant.TabIndex = 33;
            this.labelNbXGagnant.Text = "0";
            this.labelNbXGagnant.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelMorpion
            // 
            this.labelMorpion.AutoSize = true;
            this.labelMorpion.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMorpion.Location = new System.Drawing.Point(189, 24);
            this.labelMorpion.Name = "labelMorpion";
            this.labelMorpion.Size = new System.Drawing.Size(518, 119);
            this.labelMorpion.TabIndex = 50;
            this.labelMorpion.Text = "MORPION";
            // 
            // buttonC3
            // 
            this.buttonC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC3.Location = new System.Drawing.Point(490, 348);
            this.buttonC3.Name = "buttonC3";
            this.buttonC3.Size = new System.Drawing.Size(80, 80);
            this.buttonC3.TabIndex = 49;
            this.buttonC3.UseVisualStyleBackColor = true;
            this.buttonC3.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonC2
            // 
            this.buttonC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC2.Location = new System.Drawing.Point(389, 348);
            this.buttonC2.Name = "buttonC2";
            this.buttonC2.Size = new System.Drawing.Size(80, 80);
            this.buttonC2.TabIndex = 48;
            this.buttonC2.UseVisualStyleBackColor = true;
            this.buttonC2.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonB3
            // 
            this.buttonB3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB3.Location = new System.Drawing.Point(490, 243);
            this.buttonB3.Name = "buttonB3";
            this.buttonB3.Size = new System.Drawing.Size(80, 80);
            this.buttonB3.TabIndex = 46;
            this.buttonB3.UseVisualStyleBackColor = true;
            this.buttonB3.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonB2
            // 
            this.buttonB2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB2.Location = new System.Drawing.Point(389, 243);
            this.buttonB2.Name = "buttonB2";
            this.buttonB2.Size = new System.Drawing.Size(80, 80);
            this.buttonB2.TabIndex = 45;
            this.buttonB2.UseVisualStyleBackColor = true;
            this.buttonB2.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonB1
            // 
            this.buttonB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonB1.Location = new System.Drawing.Point(294, 243);
            this.buttonB1.Name = "buttonB1";
            this.buttonB1.Size = new System.Drawing.Size(80, 80);
            this.buttonB1.TabIndex = 44;
            this.buttonB1.UseVisualStyleBackColor = true;
            this.buttonB1.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonA2
            // 
            this.buttonA2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA2.Location = new System.Drawing.Point(389, 146);
            this.buttonA2.Name = "buttonA2";
            this.buttonA2.Size = new System.Drawing.Size(80, 80);
            this.buttonA2.TabIndex = 43;
            this.buttonA2.UseVisualStyleBackColor = true;
            this.buttonA2.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonA3
            // 
            this.buttonA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA3.Location = new System.Drawing.Point(490, 146);
            this.buttonA3.Name = "buttonA3";
            this.buttonA3.Size = new System.Drawing.Size(80, 80);
            this.buttonA3.TabIndex = 42;
            this.buttonA3.UseVisualStyleBackColor = true;
            this.buttonA3.Click += new System.EventHandler(this.button_Click);
            // 
            // buttonA1
            // 
            this.buttonA1.BackColor = System.Drawing.Color.Transparent;
            this.buttonA1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonA1.Location = new System.Drawing.Point(294, 146);
            this.buttonA1.Name = "buttonA1";
            this.buttonA1.Size = new System.Drawing.Size(80, 80);
            this.buttonA1.TabIndex = 41;
            this.buttonA1.UseVisualStyleBackColor = false;
            this.buttonA1.Click += new System.EventHandler(this.button_Click);
            // 
            // aProposToolStripMenuItem
            // 
            this.aProposToolStripMenuItem.Name = "aProposToolStripMenuItem";
            this.aProposToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aProposToolStripMenuItem.Text = "A propos";
            this.aProposToolStripMenuItem.Click += new System.EventHandler(this.AProposToolStripMenuItem_Click);
            // 
            // aIDEToolStripMenuItem
            // 
            this.aIDEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aProposToolStripMenuItem});
            this.aIDEToolStripMenuItem.Name = "aIDEToolStripMenuItem";
            this.aIDEToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.aIDEToolStripMenuItem.Text = "AIDE";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.QuitterToolStripMenuItem_Click);
            // 
            // NouveauJeuToolStripMenuItem
            // 
            this.NouveauJeuToolStripMenuItem.Name = "NouveauJeuToolStripMenuItem";
            this.NouveauJeuToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.NouveauJeuToolStripMenuItem.Text = "Nouveau jeu";
            // 
            // fICHIERToolStripMenuItem
            // 
            this.fICHIERToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NouveauJeuToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.fICHIERToolStripMenuItem.Name = "fICHIERToolStripMenuItem";
            this.fICHIERToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.fICHIERToolStripMenuItem.Text = "FICHIER";
            // 
            // buttonC1
            // 
            this.buttonC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC1.Location = new System.Drawing.Point(294, 348);
            this.buttonC1.Name = "buttonC1";
            this.buttonC1.Size = new System.Drawing.Size(80, 80);
            this.buttonC1.TabIndex = 47;
            this.buttonC1.UseVisualStyleBackColor = true;
            this.buttonC1.Click += new System.EventHandler(this.button_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fICHIERToolStripMenuItem,
            this.aIDEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(870, 24);
            this.menuStrip1.TabIndex = 40;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // buttonPlayMusique
            // 
            this.buttonPlayMusique.AutoSize = true;
            this.buttonPlayMusique.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonPlayMusique.Location = new System.Drawing.Point(788, 631);
            this.buttonPlayMusique.Name = "buttonPlayMusique";
            this.buttonPlayMusique.Size = new System.Drawing.Size(70, 70);
            this.buttonPlayMusique.TabIndex = 51;
            this.buttonPlayMusique.UseVisualStyleBackColor = false;
            this.buttonPlayMusique.Click += new System.EventHandler(this.buttonPlayMusique_Click);
            // 
            // buttonStopMusique
            // 
            this.buttonStopMusique.AutoSize = true;
            this.buttonStopMusique.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonStopMusique.Location = new System.Drawing.Point(12, 631);
            this.buttonStopMusique.Name = "buttonStopMusique";
            this.buttonStopMusique.Size = new System.Drawing.Size(75, 70);
            this.buttonStopMusique.TabIndex = 52;
            this.buttonStopMusique.UseVisualStyleBackColor = false;
            this.buttonStopMusique.Click += new System.EventHandler(this.buttonStopMusique_Click);
            // 
            // FormMorpion2JoueursJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(870, 789);
            this.Controls.Add(this.buttonPlayMusique);
            this.Controls.Add(this.panelOgagnant);
            this.Controls.Add(this.panelEgalite);
            this.Controls.Add(this.panelXGagnant);
            this.Controls.Add(this.buttonStopMusique);
            this.Controls.Add(this.labelMorpion);
            this.Controls.Add(this.buttonC3);
            this.Controls.Add(this.buttonC2);
            this.Controls.Add(this.buttonB3);
            this.Controls.Add(this.buttonB2);
            this.Controls.Add(this.buttonB1);
            this.Controls.Add(this.buttonA2);
            this.Controls.Add(this.buttonA3);
            this.Controls.Add(this.buttonA1);
            this.Controls.Add(this.buttonC1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FormMorpion2JoueursJeu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMorpion2JoueursJeu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMorpion2JJoueursJeu_FormClosed);
            this.Load += new System.EventHandler(this.FormMorpion2JoueursJeu_Load);
            this.panelOgagnant.ResumeLayout(false);
            this.panelOgagnant.PerformLayout();
            this.panelEgalite.ResumeLayout(false);
            this.panelEgalite.PerformLayout();
            this.panelXGagnant.ResumeLayout(false);
            this.panelXGagnant.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelNbOGagnant;
        private System.Windows.Forms.Panel panelOgagnant;
        private System.Windows.Forms.Label labelOGagnant;
        private System.Windows.Forms.Label labelNbEgalite;
        private System.Windows.Forms.Panel panelEgalite;
        private System.Windows.Forms.Label labelEgalite;
        private System.Windows.Forms.Panel panelXGagnant;
        private System.Windows.Forms.Label labelXGagnant;
        private System.Windows.Forms.Label labelNbXGagnant;
        private System.Windows.Forms.Label labelMorpion;
        private System.Windows.Forms.Button buttonC3;
        private System.Windows.Forms.Button buttonC2;
        private System.Windows.Forms.Button buttonB3;
        private System.Windows.Forms.Button buttonB2;
        private System.Windows.Forms.Button buttonB1;
        private System.Windows.Forms.Button buttonA2;
        private System.Windows.Forms.Button buttonA3;
        private System.Windows.Forms.Button buttonA1;
        private System.Windows.Forms.ToolStripMenuItem aProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aIDEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NouveauJeuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fICHIERToolStripMenuItem;
        private System.Windows.Forms.Button buttonC1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button buttonPlayMusique;
        private System.Windows.Forms.Button buttonStopMusique;
    }
}