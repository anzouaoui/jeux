﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Jeux.Vues.Pong
{
    public partial class FormPong : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        private int vitesseGaucheBalle = 10;
        private int vitesseHautBalle = 10;
        private int score = 0;
        private int meilleurScore = 0;
        static string pseudoJoueur;
        Form MdiChild;
        public FormPong()
        {
            InitializeComponent();
            timer1.Enabled = true;

            pictureBoxRaquette.Top = panelFondEcran.Bottom - (panelFondEcran.Bottom / 8);

            labelGameOver.Visible = false;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            pictureBoxRaquette.Left = Cursor.Position.X - (pictureBoxRaquette.Width / 2);
            pictureBoxBalle.Left += vitesseGaucheBalle;
            pictureBoxBalle.Top += vitesseHautBalle;

            if ((pictureBoxBalle.Bottom >= pictureBoxRaquette.Top) && (pictureBoxBalle.Bottom <= pictureBoxRaquette.Bottom) && (pictureBoxBalle.Left >= pictureBoxRaquette.Left) && (pictureBoxBalle.Right <= pictureBoxRaquette.Right))
            {
                vitesseHautBalle += 10;
                vitesseGaucheBalle += 10;
                vitesseHautBalle = -vitesseHautBalle;
                score += 1;
                labelNbScore.Text = score.ToString();
                if (score > meilleurScore)
                {
                    labelNbMeilleurScore.Text = score.ToString();
                }
                Random rdm = new Random();
                panelFondEcran.BackColor = Color.FromArgb(rdm.Next(100, 250), rdm.Next(100, 250), rdm.Next(100, 250));
            }

            if (pictureBoxBalle.Left <= panelFondEcran.Left)
            {
                vitesseGaucheBalle = -vitesseGaucheBalle;
            }

            if (pictureBoxBalle.Right >= panelFondEcran.Right)
            {
                vitesseGaucheBalle = -vitesseGaucheBalle;
            }
            if (pictureBoxBalle.Top <= panelFondEcran.Top)
            {
                vitesseHautBalle = -vitesseHautBalle;
            }
            if (pictureBoxBalle.Bottom >= panelFondEcran.Bottom)
            {
                timer1.Enabled = false;
                labelGameOver.Visible = true;
            }
        }

        private void FormPingPong_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                this.Close();
            }

            if (e.KeyData == Keys.Enter)
            {
                pictureBoxBalle.Top = 50;
                pictureBoxBalle.Left = 50;
                vitesseGaucheBalle = 10;
                vitesseHautBalle = 10;
                score = 0;
                labelNbScore.Text = "0";
                labelGameOver.Visible = false;
                panelFondEcran.BackColor = SystemColors.ActiveCaption;
                Timer1_Tick(null, null);
            }
        }

        public static void SetPseudo(string pseudo)
        {
            pseudoJoueur = pseudo;
        }

        private void FormPong_Load(object sender, EventArgs e)
        {
            labelPseudo.Text = pseudoJoueur;
            sqlConnection = new MySqlConnection(connection);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            MySqlCommand sql = new MySqlCommand("Select * from PERSONNE where pseudo = '" + labelPseudo.Text + "'");
            sqlConnection.Open();
            sql.Connection = sqlConnection;
            IDataReader reader = sql.ExecuteReader();
            while (reader.Read())
            {
                meilleurScore = Convert.ToInt32(reader.GetValue(5));
                labelNbMeilleurScore.Text = Convert.ToString(reader.GetValue(5));
            }
            sqlConnection.Close();
        }

        private void FormPong_FormClosed(object sender, FormClosedEventArgs e)
        {
            meilleurScore = Convert.ToInt32(labelNbMeilleurScore.Text);
            sqlConnection = new MySqlConnection(connection);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            sda.UpdateCommand =new MySqlCommand("Update PERSONNE set meilleurScorePong = " + meilleurScore + " where pseudo = '" + labelPseudo.Text + "'", sqlConnection);
            sqlConnection.Open();
            sda.UpdateCommand.ExecuteNonQuery();
            sqlConnection.Close();
            MdiChild = new Jeux.vues.FormFenetrePrincipale();
            MdiChild.Show();
            this.Hide();
        }
    }
}
