﻿namespace Jeux.Vues.Pong
{
    partial class FormPong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelFondEcran = new System.Windows.Forms.Panel();
            this.labelGameOver = new System.Windows.Forms.Label();
            this.pictureBoxBalle = new System.Windows.Forms.PictureBox();
            this.pictureBoxRaquette = new System.Windows.Forms.PictureBox();
            this.labelNbScore = new System.Windows.Forms.Label();
            this.labelNbMeilleurScore = new System.Windows.Forms.Label();
            this.labelMeilleurScore = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelPseudo = new System.Windows.Forms.Label();
            this.panelFondEcran.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRaquette)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFondEcran
            // 
            this.panelFondEcran.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelFondEcran.Controls.Add(this.labelGameOver);
            this.panelFondEcran.Controls.Add(this.pictureBoxBalle);
            this.panelFondEcran.Controls.Add(this.pictureBoxRaquette);
            this.panelFondEcran.Location = new System.Drawing.Point(0, 53);
            this.panelFondEcran.Name = "panelFondEcran";
            this.panelFondEcran.Size = new System.Drawing.Size(981, 644);
            this.panelFondEcran.TabIndex = 1;
            // 
            // labelGameOver
            // 
            this.labelGameOver.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelGameOver.AutoSize = true;
            this.labelGameOver.Font = new System.Drawing.Font("Berlin Sans FB", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGameOver.Location = new System.Drawing.Point(267, 212);
            this.labelGameOver.Name = "labelGameOver";
            this.labelGameOver.Size = new System.Drawing.Size(527, 180);
            this.labelGameOver.TabIndex = 41;
            this.labelGameOver.Text = "GAME OVER!!\r\n\r\nENTRER: Nouveau Jeu";
            this.labelGameOver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxBalle
            // 
            this.pictureBoxBalle.BackColor = System.Drawing.Color.OrangeRed;
            this.pictureBoxBalle.Location = new System.Drawing.Point(514, 286);
            this.pictureBoxBalle.Name = "pictureBoxBalle";
            this.pictureBoxBalle.Size = new System.Drawing.Size(25, 25);
            this.pictureBoxBalle.TabIndex = 1;
            this.pictureBoxBalle.TabStop = false;
            // 
            // pictureBoxRaquette
            // 
            this.pictureBoxRaquette.BackColor = System.Drawing.Color.DarkCyan;
            this.pictureBoxRaquette.Location = new System.Drawing.Point(422, 552);
            this.pictureBoxRaquette.Name = "pictureBoxRaquette";
            this.pictureBoxRaquette.Size = new System.Drawing.Size(150, 20);
            this.pictureBoxRaquette.TabIndex = 0;
            this.pictureBoxRaquette.TabStop = false;
            // 
            // labelNbScore
            // 
            this.labelNbScore.AutoSize = true;
            this.labelNbScore.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbScore.Location = new System.Drawing.Point(124, 6);
            this.labelNbScore.Name = "labelNbScore";
            this.labelNbScore.Size = new System.Drawing.Size(36, 37);
            this.labelNbScore.TabIndex = 40;
            this.labelNbScore.Text = "0";
            this.labelNbScore.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelNbMeilleurScore
            // 
            this.labelNbMeilleurScore.AutoSize = true;
            this.labelNbMeilleurScore.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbMeilleurScore.Location = new System.Drawing.Point(933, 6);
            this.labelNbMeilleurScore.Name = "labelNbMeilleurScore";
            this.labelNbMeilleurScore.Size = new System.Drawing.Size(36, 37);
            this.labelNbMeilleurScore.TabIndex = 39;
            this.labelNbMeilleurScore.Text = "0";
            this.labelNbMeilleurScore.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelMeilleurScore
            // 
            this.labelMeilleurScore.AutoSize = true;
            this.labelMeilleurScore.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeilleurScore.Location = new System.Drawing.Point(669, 6);
            this.labelMeilleurScore.Name = "labelMeilleurScore";
            this.labelMeilleurScore.Size = new System.Drawing.Size(258, 44);
            this.labelMeilleurScore.TabIndex = 37;
            this.labelMeilleurScore.Text = "Meilleur score:";
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.Location = new System.Drawing.Point(12, 1);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(116, 44);
            this.labelScore.TabIndex = 36;
            this.labelScore.Text = "Score:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // labelPseudo
            // 
            this.labelPseudo.AutoSize = true;
            this.labelPseudo.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPseudo.Location = new System.Drawing.Point(373, -1);
            this.labelPseudo.Name = "labelPseudo";
            this.labelPseudo.Size = new System.Drawing.Size(138, 44);
            this.labelPseudo.TabIndex = 42;
            this.labelPseudo.Text = "Pseudo";
            this.labelPseudo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FormPong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 697);
            this.Controls.Add(this.labelPseudo);
            this.Controls.Add(this.labelNbMeilleurScore);
            this.Controls.Add(this.labelNbScore);
            this.Controls.Add(this.labelMeilleurScore);
            this.Controls.Add(this.panelFondEcran);
            this.Controls.Add(this.labelScore);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormPong";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPong_FormClosed);
            this.Load += new System.EventHandler(this.FormPong_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormPingPong_KeyDown);
            this.panelFondEcran.ResumeLayout(false);
            this.panelFondEcran.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRaquette)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelFondEcran;
        private System.Windows.Forms.Label labelGameOver;
        private System.Windows.Forms.Label labelNbScore;
        private System.Windows.Forms.Label labelNbMeilleurScore;
        private System.Windows.Forms.Label labelMeilleurScore;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.PictureBox pictureBoxBalle;
        private System.Windows.Forms.PictureBox pictureBoxRaquette;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelPseudo;
    }
}