﻿namespace Jeux.Vues
{
    partial class FormMeilleurScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitre = new System.Windows.Forms.Label();
            this.dataGridViewScore = new System.Windows.Forms.DataGridView();
            this.ColumnClassement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPseudo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnScorePong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnScoreSerpent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewScore)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitre
            // 
            this.labelTitre.AutoSize = true;
            this.labelTitre.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitre.Location = new System.Drawing.Point(12, 9);
            this.labelTitre.Name = "labelTitre";
            this.labelTitre.Size = new System.Drawing.Size(1124, 119);
            this.labelTitre.TabIndex = 8;
            this.labelTitre.Text = "TABLEAUX DES SCORES";
            this.labelTitre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dataGridViewScore
            // 
            this.dataGridViewScore.AllowUserToOrderColumns = true;
            this.dataGridViewScore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewScore.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnClassement,
            this.ColumnPseudo,
            this.ColumnScorePong,
            this.ColumnScoreSerpent});
            this.dataGridViewScore.Location = new System.Drawing.Point(32, 120);
            this.dataGridViewScore.Name = "dataGridViewScore";
            this.dataGridViewScore.Size = new System.Drawing.Size(1042, 516);
            this.dataGridViewScore.TabIndex = 9;
            // 
            // ColumnClassement
            // 
            this.ColumnClassement.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnClassement.HeaderText = "CLASSEMENT";
            this.ColumnClassement.Name = "ColumnClassement";
            this.ColumnClassement.Width = 250;
            // 
            // ColumnPseudo
            // 
            this.ColumnPseudo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnPseudo.HeaderText = "PSEUDO";
            this.ColumnPseudo.Name = "ColumnPseudo";
            this.ColumnPseudo.Width = 250;
            // 
            // ColumnScorePong
            // 
            this.ColumnScorePong.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnScorePong.HeaderText = "SCORE DU PONG";
            this.ColumnScorePong.Name = "ColumnScorePong";
            this.ColumnScorePong.Width = 250;
            // 
            // ColumnScoreSerpent
            // 
            this.ColumnScoreSerpent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnScoreSerpent.HeaderText = "SCORE DU SERPENT";
            this.ColumnScoreSerpent.Name = "ColumnScoreSerpent";
            this.ColumnScoreSerpent.Width = 250;
            // 
            // FormMeilleurScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1133, 668);
            this.Controls.Add(this.dataGridViewScore);
            this.Controls.Add(this.labelTitre);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMeilleurScore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "tableau des scores";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMeilleurScore_FormClosed);
            this.Load += new System.EventHandler(this.FormMeilleurScore_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewScore)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitre;
        private System.Windows.Forms.DataGridView dataGridViewScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnClassement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPseudo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnScorePong;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnScoreSerpent;
    }
}