﻿namespace Jeux.vues
{
    partial class FormInscription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonInscription = new System.Windows.Forms.Button();
            this.textBoxInscriptionMotDePasse = new System.Windows.Forms.TextBox();
            this.textBoxInscriptionNom = new System.Windows.Forms.TextBox();
            this.labelInscriptionMotDePasse = new System.Windows.Forms.Label();
            this.labelInscriptionNom = new System.Windows.Forms.Label();
            this.labelJeux = new System.Windows.Forms.Label();
            this.labelPseudo = new System.Windows.Forms.Label();
            this.textBoxInscriptionPseudo = new System.Windows.Forms.TextBox();
            this.labelInscirptionConfMotDePasse = new System.Windows.Forms.Label();
            this.textBoxInscriptionConfMotDePasse = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonInscription
            // 
            this.buttonInscription.AutoSize = true;
            this.buttonInscription.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInscription.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInscription.Location = new System.Drawing.Point(688, 724);
            this.buttonInscription.Name = "buttonInscription";
            this.buttonInscription.Size = new System.Drawing.Size(256, 56);
            this.buttonInscription.TabIndex = 30;
            this.buttonInscription.Text = "INSCRIPTION";
            this.buttonInscription.UseVisualStyleBackColor = false;
            this.buttonInscription.Click += new System.EventHandler(this.ButtonInscription_Click);
            // 
            // textBoxInscriptionMotDePasse
            // 
            this.textBoxInscriptionMotDePasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInscriptionMotDePasse.Location = new System.Drawing.Point(323, 333);
            this.textBoxInscriptionMotDePasse.Name = "textBoxInscriptionMotDePasse";
            this.textBoxInscriptionMotDePasse.Size = new System.Drawing.Size(394, 40);
            this.textBoxInscriptionMotDePasse.TabIndex = 29;
            this.textBoxInscriptionMotDePasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInscriptionMotDePasse.Leave += new System.EventHandler(this.TextBoxInscriptionMotDePasse_Leave);
            // 
            // textBoxInscriptionNom
            // 
            this.textBoxInscriptionNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInscriptionNom.Location = new System.Drawing.Point(323, 192);
            this.textBoxInscriptionNom.Name = "textBoxInscriptionNom";
            this.textBoxInscriptionNom.Size = new System.Drawing.Size(394, 40);
            this.textBoxInscriptionNom.TabIndex = 28;
            this.textBoxInscriptionNom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInscriptionNom.Leave += new System.EventHandler(this.TextBoxInscriptionNom_Leave);
            // 
            // labelInscriptionMotDePasse
            // 
            this.labelInscriptionMotDePasse.AutoSize = true;
            this.labelInscriptionMotDePasse.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInscriptionMotDePasse.Location = new System.Drawing.Point(315, 286);
            this.labelInscriptionMotDePasse.Name = "labelInscriptionMotDePasse";
            this.labelInscriptionMotDePasse.Size = new System.Drawing.Size(277, 44);
            this.labelInscriptionMotDePasse.TabIndex = 27;
            this.labelInscriptionMotDePasse.Text = "MOT DE PASSE:";
            // 
            // labelInscriptionNom
            // 
            this.labelInscriptionNom.AutoSize = true;
            this.labelInscriptionNom.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInscriptionNom.Location = new System.Drawing.Point(315, 145);
            this.labelInscriptionNom.Name = "labelInscriptionNom";
            this.labelInscriptionNom.Size = new System.Drawing.Size(115, 44);
            this.labelInscriptionNom.TabIndex = 26;
            this.labelInscriptionNom.Text = "NOM:";
            // 
            // labelJeux
            // 
            this.labelJeux.AutoSize = true;
            this.labelJeux.Font = new System.Drawing.Font("Berlin Sans FB", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJeux.Location = new System.Drawing.Point(298, 21);
            this.labelJeux.Name = "labelJeux";
            this.labelJeux.Size = new System.Drawing.Size(442, 79);
            this.labelJeux.TabIndex = 25;
            this.labelJeux.Text = "INSCRIPTION";
            // 
            // labelPseudo
            // 
            this.labelPseudo.AutoSize = true;
            this.labelPseudo.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPseudo.Location = new System.Drawing.Point(315, 416);
            this.labelPseudo.Name = "labelPseudo";
            this.labelPseudo.Size = new System.Drawing.Size(170, 44);
            this.labelPseudo.TabIndex = 31;
            this.labelPseudo.Text = "PSEUDO:";
            // 
            // textBoxInscriptionPseudo
            // 
            this.textBoxInscriptionPseudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInscriptionPseudo.Location = new System.Drawing.Point(323, 463);
            this.textBoxInscriptionPseudo.Name = "textBoxInscriptionPseudo";
            this.textBoxInscriptionPseudo.Size = new System.Drawing.Size(394, 40);
            this.textBoxInscriptionPseudo.TabIndex = 32;
            this.textBoxInscriptionPseudo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInscriptionPseudo.Leave += new System.EventHandler(this.TextBoxInsciprtionPseudo_Leave);
            // 
            // labelInscirptionConfMotDePasse
            // 
            this.labelInscirptionConfMotDePasse.AutoSize = true;
            this.labelInscirptionConfMotDePasse.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInscirptionConfMotDePasse.Location = new System.Drawing.Point(315, 554);
            this.labelInscirptionConfMotDePasse.Name = "labelInscirptionConfMotDePasse";
            this.labelInscirptionConfMotDePasse.Size = new System.Drawing.Size(496, 44);
            this.labelInscirptionConfMotDePasse.TabIndex = 33;
            this.labelInscirptionConfMotDePasse.Text = "CONFIRMER MOT DE PASSE:";
            // 
            // textBoxInscriptionConfMotDePasse
            // 
            this.textBoxInscriptionConfMotDePasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInscriptionConfMotDePasse.Location = new System.Drawing.Point(323, 617);
            this.textBoxInscriptionConfMotDePasse.Name = "textBoxInscriptionConfMotDePasse";
            this.textBoxInscriptionConfMotDePasse.Size = new System.Drawing.Size(394, 40);
            this.textBoxInscriptionConfMotDePasse.TabIndex = 34;
            this.textBoxInscriptionConfMotDePasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInscriptionConfMotDePasse.Leave += new System.EventHandler(this.TextBoxInscriptionConfMotDePasse_Leave);
            // 
            // FormInscription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1044, 792);
            this.Controls.Add(this.textBoxInscriptionConfMotDePasse);
            this.Controls.Add(this.labelInscirptionConfMotDePasse);
            this.Controls.Add(this.textBoxInscriptionPseudo);
            this.Controls.Add(this.labelPseudo);
            this.Controls.Add(this.buttonInscription);
            this.Controls.Add(this.textBoxInscriptionMotDePasse);
            this.Controls.Add(this.textBoxInscriptionNom);
            this.Controls.Add(this.labelInscriptionMotDePasse);
            this.Controls.Add(this.labelInscriptionNom);
            this.Controls.Add(this.labelJeux);
            this.Name = "FormInscription";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormInscription";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormInscription_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonInscription;
        private System.Windows.Forms.TextBox textBoxInscriptionMotDePasse;
        private System.Windows.Forms.TextBox textBoxInscriptionNom;
        private System.Windows.Forms.Label labelInscriptionMotDePasse;
        private System.Windows.Forms.Label labelInscriptionNom;
        private System.Windows.Forms.Label labelJeux;
        private System.Windows.Forms.Label labelPseudo;
        private System.Windows.Forms.TextBox textBoxInscriptionPseudo;
        private System.Windows.Forms.Label labelInscirptionConfMotDePasse;
        private System.Windows.Forms.TextBox textBoxInscriptionConfMotDePasse;
    }
}