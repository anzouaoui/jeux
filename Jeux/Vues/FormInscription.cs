﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Jeux.Classes;

namespace Jeux.vues
{
    public partial class FormInscription : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        Form MdiChild;

        public FormInscription()
        {
            InitializeComponent();
        }

        private void ButtonInscription_Click(object sender, EventArgs e)
        {
            sqlConnection = new MySqlConnection(connection);
            sqlConnection.Open();
            MySqlCommand sql = new MySqlCommand("Insert into PERSONNE (nom_pers, pseudo, mdp_pers, confMdp) values ('" + textBoxInscriptionNom.Text + "','" + textBoxInscriptionPseudo.Text+ "','" + textBoxInscriptionMotDePasse.Text + "','" + textBoxInscriptionConfMotDePasse.Text +  "')", sqlConnection);
            if (textBoxInscriptionMotDePasse.Text == textBoxInscriptionConfMotDePasse.Text)
            {
                sql.ExecuteNonQuery();
                this.Hide();
                MdiChild = new FormConnexion();
                MdiChild.Show();
            }
            else
            {
                MessageBox.Show("Les deux mot de passe ne correspondent pas\n");
            }
            
            sqlConnection.Close();
        }

        private void TextBoxInscriptionNom_Leave(object sender, EventArgs e)
        {
            if (this.textBoxInscriptionNom.Text == "")
            {
                MessageBox.Show("Saisir votre nom");
                textBoxInscriptionNom.Text = "";
                textBoxInscriptionNom.Focus();
            }
        }

        private void TextBoxInsciprtionPseudo_Leave(object sender, EventArgs e)
        {
            if (this.textBoxInscriptionPseudo.Text == "")
            {
                MessageBox.Show("Saisir votre pseudo");
                textBoxInscriptionPseudo.Text = "";
                textBoxInscriptionPseudo.Focus();
            }
        }

        private void TextBoxInscriptionMotDePasse_Leave(object sender, EventArgs e)
        {
            if (this.textBoxInscriptionMotDePasse.Text == "")
            {
                MessageBox.Show("Saisir votre mot de passe");
                textBoxInscriptionMotDePasse.Text = "";
                textBoxInscriptionMotDePasse.Focus();
            }
        }

        private void TextBoxInscriptionConfMotDePasse_Leave(object sender, EventArgs e)
        {
            if (this.textBoxInscriptionConfMotDePasse.Text == "")
            {
                MessageBox.Show("Saisir votre mot de passe");
                textBoxInscriptionConfMotDePasse.Text = "";
                textBoxInscriptionConfMotDePasse.Focus();
            }
        }

        private void FormInscription_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            MdiChild = new FormConnexion();
            MdiChild.Show();
        }
    }
}
