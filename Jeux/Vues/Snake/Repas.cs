﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeux.Vues.Snake
{
    class Repas
    {
        private int X;
        private int Y;
        private int largeur;
        private int longueur;
        private SolidBrush pinceauRepas;
        public Rectangle repas;

        public Repas(Random rdm)
        {
            X = rdm.Next(0, 29) * 10;
            Y = rdm.Next(0, 29) * 10;

            pinceauRepas = new SolidBrush(Color.Black);

            largeur = 10;
            longueur = 10;

            repas = new Rectangle(X, Y, largeur, longueur);
        }

        public void localisationRepas(Random rdm)
        {
            X = rdm.Next(0, 29) * 10;
            Y = rdm.Next(0, 29) * 10;
        }

        public void dessinerRepas(Graphics papier)
        {
            repas.X = X;
            repas.Y = Y;

            papier.FillRectangle(pinceauRepas, repas);
        }
    }
}
