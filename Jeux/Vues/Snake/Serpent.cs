﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeux.Vues.Snake
{
    public class Serpent
    {
        private Rectangle[] serpent;
        private SolidBrush pinceauCorps;
        private SolidBrush pinceauTete;
        private int X, Y, largeur, longueur;


        public Rectangle[] SerpentRec
        {
            get { return serpent; }
        }

        public Serpent()
        {
            serpent = new Rectangle[3];
            pinceauCorps = new SolidBrush(Color.Coral);
            pinceauTete = new SolidBrush(Color.Azure);
            X = 20;
            Y = 20;
            largeur = 10;
            longueur = 10;

            for(int i=0;i<serpent.Length; i++)
            {
                serpent[i] = new Rectangle(X, Y, largeur, longueur);
                X -= 10; 
            }
        }

        /// <summary>
        /// Placer le serpent au début du jeu
        /// </summary>
        /// <param name="papier"></param>
        public void dessinerSerpent(Graphics papier)
        {
            foreach(Rectangle rectangleCourant in serpent)
            {
                papier.FillRectangle(pinceauCorps, rectangleCourant);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void dessinerSerpent()
        {
            for (int i = serpent.Length - 1; i > 0; i--)
            {
                serpent[i] = serpent[i - 1];
            }
        }

        /// <summary>
        /// Déplacement du serpent vers le bas
        /// </summary>
        public void mouvementVersLeBas()
        {
            dessinerSerpent();
            serpent[0].Y += 10;
        }

        /// <summary>
        /// Déplacement du serpent vers le haut
        /// </summary>
        public void mouvementVersLeHaut()
        {
            dessinerSerpent();
            serpent[0].Y -= 10;
        }

        /// <summary>
        /// Déplacement du serpent vers la gauche
        /// </summary>
        public void mouvementVersLaGauche()
        {
            dessinerSerpent();
            serpent[0].X -= 10;
        }

        /// <summary>
        /// Déplacement du serpent vers la droite
        /// </summary>
        public void mouvementVersLaDroite()
        {
            dessinerSerpent();
            serpent[0].X += 10;
        }

        public void grandir()
        {
            List<Rectangle> collectionRec = serpent.ToList();
            collectionRec.Add(new Rectangle(serpent[serpent.Length - 1].X, serpent[serpent.Length - 1].Y, largeur, longueur));
            serpent = collectionRec.ToArray();
        }
    }
}
