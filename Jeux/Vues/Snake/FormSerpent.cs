﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Jeux.Vues.Snake
{
    public partial class FormSerpent : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        Form MdiChild;
        Graphics papier;
        Serpent serpent = new Serpent();
        Random rm = new Random();
        Repas repas;
        int score = 0;
        int meilleurScore = 0;
        bool gauche = false;
        bool droite = false;
        bool haut = false;
        bool bas = false;
        static string pseudoJoueur;
        public FormSerpent()
        {
            InitializeComponent();
            repas = new Repas(rm);
        }

        private void FormSnake_Paint(object sender, PaintEventArgs e)
        {
            papier = e.Graphics;
            serpent.dessinerSerpent(papier);
            repas.dessinerRepas(papier);
            
        }

        private void FormSnake_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down && haut == false)
            {
                bas = true;
                haut = false;
                gauche = false;
                droite = false;
            }
            if (e.KeyData == Keys.Up && bas == false)
            {
                haut = true;
                bas = false;
                gauche = false;
                droite = false;
            }
            if (e.KeyData == Keys.Left && droite == false)
            {
                gauche = true;
                haut = false;
                bas = false;
                droite = false;
            }
            if (e.KeyData == Keys.Right && gauche == false)
            {
                droite = true;
                haut = false;
                gauche = false;
                bas = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelNbScore.Text = Convert.ToString(score);
            if (bas)
            {
                serpent.mouvementVersLeBas();
            }
            if (haut)
            {
                serpent.mouvementVersLeHaut();
            }
            if (gauche)
            {
                serpent.mouvementVersLaGauche();
            }
            if (droite)
            {
                serpent.mouvementVersLaDroite();
            }

            for (int i = 0; i < serpent.SerpentRec.Length; i++)
            {
                if (serpent.SerpentRec[i].IntersectsWith(repas.repas))
                {
                    score++;
                    if (score > meilleurScore)
                    {
                        labelNbMeilleurScore.Text = score.ToString();
                    }
                    serpent.grandir();
                    repas.localisationRepas(rm);
                    timer1.Interval -= 5;
                }
            }
            collision();
            this.Invalidate();
        }

        public void collision()
        {
            for (int i = 1; i < serpent.SerpentRec.Length; i++)
            {
                if (serpent.SerpentRec[0].IntersectsWith(serpent.SerpentRec[i]))
                {
                    timer1.Enabled = false;
                    MessageBox.Show("PERDU!!!");
                    buttonNouveauJeu.Visible = true;
                    buttonQuitter.Visible = true;
                }
            }

            if (serpent.SerpentRec[0].X < 0 || serpent.SerpentRec[0].X > 500)
            {
                timer1.Enabled = false;
                MessageBox.Show("PERDU!!!");
                buttonNouveauJeu.Visible = true;
                buttonQuitter.Visible = true;
            }

            if (serpent.SerpentRec[0].Y < 0 || serpent.SerpentRec[0].Y > 500)
            {
                timer1.Enabled = false;
                MessageBox.Show("PERDU!!!");
                buttonNouveauJeu.Visible = true;
                buttonQuitter.Visible = true;
            }
        }

        public void redemarrer()
        {
            score = 0;
            labelNbScore.Text = "0";
            buttonNouveauJeu.Visible = false;
            buttonQuitter.Visible = false;
            timer1.Interval = 200;

        }

        private void FormSerpent_Load(object sender, EventArgs e)
        {
            buttonNouveauJeu.Visible = false;
            buttonQuitter.Visible = false;
            labelPseudo.Text = pseudoJoueur;
            sqlConnection = new MySqlConnection(connection);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            MySqlCommand sql = new MySqlCommand("Select * from PERSONNE where pseudo = '" + labelPseudo.Text + "'");
            sqlConnection.Open();
            sql.Connection = sqlConnection;
            IDataReader reader = sql.ExecuteReader();
            while (reader.Read())
            {
                meilleurScore = Convert.ToInt32(reader.GetValue(6));
                labelNbMeilleurScore.Text = Convert.ToString(reader.GetValue(6));
            }
            sqlConnection.Close();
        }

        private void buttonQuitter_Click(object sender, EventArgs e)
        {
            meilleurScore = Convert.ToInt32(labelNbMeilleurScore.Text);
            sqlConnection = new MySqlConnection(connection);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            sda.UpdateCommand = new MySqlCommand("Update PERSONNE set meilleurScoreSerpent = " + meilleurScore + " where pseudo = '" + labelPseudo.Text + "'", sqlConnection);
            sqlConnection.Open();
            sda.UpdateCommand.ExecuteNonQuery();
            sqlConnection.Close();
            Application.Exit();
        }

        private void buttonNouveauJeu_Click(object sender, EventArgs e)
        {
            redemarrer();
        }

        private void FormSerpent_FormClosed(object sender, FormClosedEventArgs e)
        {
            meilleurScore = Convert.ToInt32(labelNbMeilleurScore.Text);
            sqlConnection = new MySqlConnection(connection);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            sda.UpdateCommand = new MySqlCommand("Update PERSONNE set meilleurScoreSerpent = " + meilleurScore + " where pseudo = '" + labelPseudo.Text + "'", sqlConnection);
            sqlConnection.Open();
            sda.UpdateCommand.ExecuteNonQuery();
            sqlConnection.Close();
            MdiChild = new Jeux.vues.FormFenetrePrincipale();
            MdiChild.Show();
            this.Hide();
        }

        public static void SetPseudo(string pseudo)
        {
            pseudoJoueur = pseudo;
        }
    }
}
