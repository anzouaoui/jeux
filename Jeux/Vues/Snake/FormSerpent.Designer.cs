﻿namespace Jeux.Vues.Snake
{
    partial class FormSerpent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelScore = new System.Windows.Forms.Panel();
            this.buttonQuitter = new System.Windows.Forms.Button();
            this.buttonNouveauJeu = new System.Windows.Forms.Button();
            this.labelNbMeilleurScore = new System.Windows.Forms.Label();
            this.labelMeilleurScore = new System.Windows.Forms.Label();
            this.labelPseudo = new System.Windows.Forms.Label();
            this.labelNbScore = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.panelScore.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panelScore
            // 
            this.panelScore.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelScore.Controls.Add(this.labelNbMeilleurScore);
            this.panelScore.Controls.Add(this.labelMeilleurScore);
            this.panelScore.Controls.Add(this.labelPseudo);
            this.panelScore.Controls.Add(this.labelNbScore);
            this.panelScore.Controls.Add(this.labelScore);
            this.panelScore.Location = new System.Drawing.Point(509, 1);
            this.panelScore.Name = "panelScore";
            this.panelScore.Size = new System.Drawing.Size(388, 500);
            this.panelScore.TabIndex = 1;
            // 
            // buttonQuitter
            // 
            this.buttonQuitter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonQuitter.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonQuitter.Location = new System.Drawing.Point(64, 335);
            this.buttonQuitter.Name = "buttonQuitter";
            this.buttonQuitter.Size = new System.Drawing.Size(388, 56);
            this.buttonQuitter.TabIndex = 47;
            this.buttonQuitter.Text = "QUITTER";
            this.buttonQuitter.UseVisualStyleBackColor = false;
            this.buttonQuitter.Click += new System.EventHandler(this.buttonQuitter_Click);
            // 
            // buttonNouveauJeu
            // 
            this.buttonNouveauJeu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonNouveauJeu.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNouveauJeu.Location = new System.Drawing.Point(70, 129);
            this.buttonNouveauJeu.Name = "buttonNouveauJeu";
            this.buttonNouveauJeu.Size = new System.Drawing.Size(382, 56);
            this.buttonNouveauJeu.TabIndex = 46;
            this.buttonNouveauJeu.Text = "NOUVEAU JEU";
            this.buttonNouveauJeu.UseVisualStyleBackColor = false;
            this.buttonNouveauJeu.Click += new System.EventHandler(this.buttonNouveauJeu_Click);
            // 
            // labelNbMeilleurScore
            // 
            this.labelNbMeilleurScore.AutoSize = true;
            this.labelNbMeilleurScore.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbMeilleurScore.Location = new System.Drawing.Point(185, 353);
            this.labelNbMeilleurScore.Name = "labelNbMeilleurScore";
            this.labelNbMeilleurScore.Size = new System.Drawing.Size(36, 37);
            this.labelNbMeilleurScore.TabIndex = 45;
            this.labelNbMeilleurScore.Text = "0";
            this.labelNbMeilleurScore.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelMeilleurScore
            // 
            this.labelMeilleurScore.AutoSize = true;
            this.labelMeilleurScore.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeilleurScore.Location = new System.Drawing.Point(78, 299);
            this.labelMeilleurScore.Name = "labelMeilleurScore";
            this.labelMeilleurScore.Size = new System.Drawing.Size(258, 44);
            this.labelMeilleurScore.TabIndex = 44;
            this.labelMeilleurScore.Text = "Meilleur score:";
            // 
            // labelPseudo
            // 
            this.labelPseudo.AutoSize = true;
            this.labelPseudo.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPseudo.Location = new System.Drawing.Point(126, 0);
            this.labelPseudo.Name = "labelPseudo";
            this.labelPseudo.Size = new System.Drawing.Size(138, 44);
            this.labelPseudo.TabIndex = 43;
            this.labelPseudo.Text = "Pseudo";
            this.labelPseudo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelNbScore
            // 
            this.labelNbScore.AutoSize = true;
            this.labelNbScore.Font = new System.Drawing.Font("Berlin Sans FB", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNbScore.Location = new System.Drawing.Point(185, 182);
            this.labelNbScore.Name = "labelNbScore";
            this.labelNbScore.Size = new System.Drawing.Size(36, 37);
            this.labelNbScore.TabIndex = 41;
            this.labelNbScore.Text = "0";
            this.labelNbScore.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.Location = new System.Drawing.Point(148, 128);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(116, 44);
            this.labelScore.TabIndex = 37;
            this.labelScore.Text = "Score:";
            // 
            // FormSerpent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(897, 502);
            this.Controls.Add(this.buttonQuitter);
            this.Controls.Add(this.panelScore);
            this.Controls.Add(this.buttonNouveauJeu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSerpent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jeux du Snake";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormSerpent_FormClosed);
            this.Load += new System.EventHandler(this.FormSerpent_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FormSnake_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormSnake_KeyDown);
            this.panelScore.ResumeLayout(false);
            this.panelScore.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panelScore;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelNbScore;
        private System.Windows.Forms.Label labelPseudo;
        private System.Windows.Forms.Label labelMeilleurScore;
        private System.Windows.Forms.Label labelNbMeilleurScore;
        private System.Windows.Forms.Button buttonQuitter;
        private System.Windows.Forms.Button buttonNouveauJeu;
    }
}