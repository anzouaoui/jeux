﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Jeux.vues;

namespace Jeux.Vues
{
    public partial class FormModifierUtilisateur : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        static string pseudo;
        Form MdiChild;

        public FormModifierUtilisateur()
        {
            InitializeComponent();
        }

        private void FormModifierUtilisateur_Load(object sender, EventArgs e)
        {
            sqlConnection = new MySqlConnection(connection);
            sqlConnection.Open();
            textBoxPseudoUtilisateur.Text = pseudo;
            MySqlCommand sql = new MySqlCommand("Select * from PERSONNE where pseudo = '" + textBoxPseudoUtilisateur.Text + "'");
            sql.Connection = sqlConnection;
            IDataReader reader = sql.ExecuteReader();
            while (reader.Read())
            {
                textBoxNomUtilisateur.Text = Convert.ToString(reader.GetValue(1));
                textBoxMotDePasseUtilisateur.Text = Convert.ToString(reader.GetValue(3));
            }
            sqlConnection.Close();
        }

        public static void SetPseudo(string pseudoJoueur)
        {
            pseudo = pseudoJoueur;
        }

        private void ButtonModifier_Click(object sender, EventArgs e)
        {
            sqlConnection = new MySqlConnection(connection);
            MySqlDataAdapter sda = new MySqlDataAdapter();
            if (textBoxMotDePasseUtilisateur.Text == textBoxConfMotDePasse.Text)
            {
                sda.UpdateCommand = new MySqlCommand("Update PERSONNE set nom_pers = '" + textBoxNomUtilisateur.Text + "', pseudo = '" + textBoxPseudoUtilisateur.Text + "', mdp_pers = '" + textBoxMotDePasseUtilisateur.Text + "', confMdp = '" + textBoxConfMotDePasse.Text + "' where pseudo = '" + textBoxPseudoUtilisateur.Text + "'", sqlConnection);
                sqlConnection.Open();
                sda.UpdateCommand.ExecuteNonQuery();
                MdiChild = new FormCompteUtilisateur();
                MdiChild.Show();
                this.Hide();
                sqlConnection.Close();
            }
            else
            {
                MessageBox.Show("Veuillez confirmer votre mot de passe");
            }
        }

        private void FormModifierUtilisateur_FormClosed(object sender, FormClosedEventArgs e)
        {
            MdiChild = new FormCompteUtilisateur();
            MdiChild.Show();
            this.Hide();
        }
    }
}
