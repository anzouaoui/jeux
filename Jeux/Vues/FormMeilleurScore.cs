﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Jeux.Vues
{
    public partial class FormMeilleurScore : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        private MySqlDataReader dataReader;
        private BindingSource bindingSource = new BindingSource();
        static string pseudo;
        Form MdiChild;
        private int classement = 0;

        public FormMeilleurScore()
        {
            InitializeComponent();
        }

        private void FormMeilleurScore_Load(object sender, EventArgs e)
        {
            RecupererDonnees();
        }

        private void RecupererDonnees()
        {
            dataGridViewScore.Rows.Clear();
            sqlConnection = new MySqlConnection(connection);
            sqlConnection.Open();
            MySqlCommand sql = new MySqlCommand("Select pseudo, meilleurScorePong, meilleurScoreSerpent from PERSONNE order by meilleurScorePong DESC, meilleurScoreSerpent DESC");
            sql.Connection = sqlConnection;
            dataReader = sql.ExecuteReader();
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    classement++;
                    dataGridViewScore.Rows.Add(classement,dataReader[0].ToString(), dataReader[1].ToString(), dataReader[2].ToString());
                }
            }
            sqlConnection.Close();
        }

        private void FormMeilleurScore_FormClosed(object sender, FormClosedEventArgs e)
        {
            MdiChild = new Jeux.vues.FormConnexion();
            MdiChild.Show();
            this.Hide();
        }
    }
}
