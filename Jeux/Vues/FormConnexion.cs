﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Jeux.Vues;

namespace Jeux.vues
{
    public partial class FormConnexion : Form
    {
        string connection = "SERVER=192.168.0.20; DATABASE=jeux; UID=zouaoui; PASSWORD=12/05/1996";
        private MySqlConnection sqlConnection;
        Form MdiChild;

        public FormConnexion()
        {
            InitializeComponent();
        }

        private void ButtonConnexion_Click(object sender, EventArgs e)
        {
            sqlConnection = new MySqlConnection(connection);
            string pseudo = textBoxPseudo.Text;
            sqlConnection.Open();
            MySqlDataAdapter sda = new MySqlDataAdapter("Select count(*) from PERSONNE where pseudo='" + textBoxPseudo.Text + "' and mdp_pers='" + textBoxMotDePasse.Text + "'", sqlConnection);
            DataTable dataTable = new DataTable();
            sda.Fill(dataTable);
            if (dataTable.Rows[0][0].ToString() == "1")
            {
                this.Hide();

                Jeux.Vues.Morpion.Morpion2Joueurs.FormMorpion2Joueurs.SetPseudo(textBoxPseudo.Text);
                Jeux.Vues.Morpion.MorpionAi.FormMorpionAi.SetPseudo(textBoxPseudo.Text);
                Jeux.Vues.Pong.FormPong.SetPseudo(textBoxPseudo.Text);
                Jeux.Vues.Pendu.FormPendu.SetPseudo(textBoxPseudo.Text);
                Jeux.Vues.Snake.FormSerpent.SetPseudo(textBoxPseudo.Text);
                FormFenetrePrincipale.SetPseudo(textBoxPseudo.Text);
                FormCompteUtilisateur.SetPseudo(textBoxPseudo.Text);
                FormModifierUtilisateur.SetPseudo(textBoxPseudo.Text);
                MdiChild = new FormFenetrePrincipale();
                MdiChild.Show();
            }
            else
            {
                MessageBox.Show("Erreur d'authentification.\n");
            }
        }

        private void ButtonInscription_Click(object sender, EventArgs e)
        {
            this.Hide();
            MdiChild = new FormInscription();
            MdiChild.Show();
        }

        private void FormConnexion_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void FormConnexion_Load(object sender, EventArgs e)
        {

        }

        private void TextBoxNomUitlisateur_Leave(object sender, EventArgs e)
        {
        }

        private void textBoxMotDePasse_Leave(object sender, EventArgs e)
        {
        }
    }
}
