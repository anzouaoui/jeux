﻿namespace Jeux.Vues
{
    partial class FormCompteUtilisateur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitre = new System.Windows.Forms.Label();
            this.labelNomUtilisateur = new System.Windows.Forms.Label();
            this.labelPseudoUtilisateur = new System.Windows.Forms.Label();
            this.labelMotDePasseUtilisateur = new System.Windows.Forms.Label();
            this.buttonModifier = new System.Windows.Forms.Button();
            this.textBoxNomUtilisateur = new System.Windows.Forms.TextBox();
            this.textBoxPseudoUtilisateur = new System.Windows.Forms.TextBox();
            this.textBoxMotDePasseUtilisateur = new System.Windows.Forms.TextBox();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTitre
            // 
            this.labelTitre.AutoSize = true;
            this.labelTitre.Font = new System.Drawing.Font("Berlin Sans FB", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitre.Location = new System.Drawing.Point(105, 9);
            this.labelTitre.Name = "labelTitre";
            this.labelTitre.Size = new System.Drawing.Size(695, 119);
            this.labelTitre.TabIndex = 8;
            this.labelTitre.Text = "MON COMPTE";
            this.labelTitre.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelTitre.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // labelNomUtilisateur
            // 
            this.labelNomUtilisateur.AutoSize = true;
            this.labelNomUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNomUtilisateur.Location = new System.Drawing.Point(305, 146);
            this.labelNomUtilisateur.Name = "labelNomUtilisateur";
            this.labelNomUtilisateur.Size = new System.Drawing.Size(115, 44);
            this.labelNomUtilisateur.TabIndex = 21;
            this.labelNomUtilisateur.Text = "NOM:";
            this.labelNomUtilisateur.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // labelPseudoUtilisateur
            // 
            this.labelPseudoUtilisateur.AutoSize = true;
            this.labelPseudoUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPseudoUtilisateur.Location = new System.Drawing.Point(305, 275);
            this.labelPseudoUtilisateur.Name = "labelPseudoUtilisateur";
            this.labelPseudoUtilisateur.Size = new System.Drawing.Size(170, 44);
            this.labelPseudoUtilisateur.TabIndex = 22;
            this.labelPseudoUtilisateur.Text = "PSEUDO:";
            this.labelPseudoUtilisateur.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // labelMotDePasseUtilisateur
            // 
            this.labelMotDePasseUtilisateur.AutoSize = true;
            this.labelMotDePasseUtilisateur.Font = new System.Drawing.Font("Berlin Sans FB", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotDePasseUtilisateur.Location = new System.Drawing.Point(305, 407);
            this.labelMotDePasseUtilisateur.Name = "labelMotDePasseUtilisateur";
            this.labelMotDePasseUtilisateur.Size = new System.Drawing.Size(277, 44);
            this.labelMotDePasseUtilisateur.TabIndex = 23;
            this.labelMotDePasseUtilisateur.Text = "MOT DE PASSE:";
            this.labelMotDePasseUtilisateur.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // buttonModifier
            // 
            this.buttonModifier.AutoSize = true;
            this.buttonModifier.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonModifier.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonModifier.Location = new System.Drawing.Point(154, 576);
            this.buttonModifier.Name = "buttonModifier";
            this.buttonModifier.Size = new System.Drawing.Size(234, 53);
            this.buttonModifier.TabIndex = 25;
            this.buttonModifier.Text = "MODIFIER";
            this.buttonModifier.UseVisualStyleBackColor = false;
            this.buttonModifier.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // textBoxNomUtilisateur
            // 
            this.textBoxNomUtilisateur.Enabled = false;
            this.textBoxNomUtilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNomUtilisateur.Location = new System.Drawing.Point(313, 193);
            this.textBoxNomUtilisateur.Name = "textBoxNomUtilisateur";
            this.textBoxNomUtilisateur.Size = new System.Drawing.Size(346, 40);
            this.textBoxNomUtilisateur.TabIndex = 26;
            this.textBoxNomUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxNomUtilisateur.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // textBoxPseudoUtilisateur
            // 
            this.textBoxPseudoUtilisateur.Enabled = false;
            this.textBoxPseudoUtilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPseudoUtilisateur.Location = new System.Drawing.Point(313, 322);
            this.textBoxPseudoUtilisateur.Name = "textBoxPseudoUtilisateur";
            this.textBoxPseudoUtilisateur.Size = new System.Drawing.Size(346, 40);
            this.textBoxPseudoUtilisateur.TabIndex = 27;
            this.textBoxPseudoUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPseudoUtilisateur.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // textBoxMotDePasseUtilisateur
            // 
            this.textBoxMotDePasseUtilisateur.Enabled = false;
            this.textBoxMotDePasseUtilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMotDePasseUtilisateur.Location = new System.Drawing.Point(313, 454);
            this.textBoxMotDePasseUtilisateur.Name = "textBoxMotDePasseUtilisateur";
            this.textBoxMotDePasseUtilisateur.Size = new System.Drawing.Size(346, 40);
            this.textBoxMotDePasseUtilisateur.TabIndex = 28;
            this.textBoxMotDePasseUtilisateur.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxMotDePasseUtilisateur.Click += new System.EventHandler(this.ButtonModifier_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.AutoSize = true;
            this.buttonSupprimer.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSupprimer.Font = new System.Drawing.Font("Modern No. 20", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSupprimer.Location = new System.Drawing.Point(496, 576);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(240, 53);
            this.buttonSupprimer.TabIndex = 29;
            this.buttonSupprimer.Text = "SUPPRIMER";
            this.buttonSupprimer.UseVisualStyleBackColor = false;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // FormCompteUtilisateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(935, 641);
            this.Controls.Add(this.buttonSupprimer);
            this.Controls.Add(this.textBoxMotDePasseUtilisateur);
            this.Controls.Add(this.textBoxPseudoUtilisateur);
            this.Controls.Add(this.textBoxNomUtilisateur);
            this.Controls.Add(this.buttonModifier);
            this.Controls.Add(this.labelMotDePasseUtilisateur);
            this.Controls.Add(this.labelPseudoUtilisateur);
            this.Controls.Add(this.labelNomUtilisateur);
            this.Controls.Add(this.labelTitre);
            this.Name = "FormCompteUtilisateur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormCompteUtilisateur";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormCompteUtilisateur_FormClosed);
            this.Load += new System.EventHandler(this.FormCompteUtilisateur_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitre;
        private System.Windows.Forms.Label labelNomUtilisateur;
        private System.Windows.Forms.Label labelPseudoUtilisateur;
        private System.Windows.Forms.Label labelMotDePasseUtilisateur;
        private System.Windows.Forms.Button buttonModifier;
        private System.Windows.Forms.TextBox textBoxNomUtilisateur;
        private System.Windows.Forms.TextBox textBoxPseudoUtilisateur;
        private System.Windows.Forms.TextBox textBoxMotDePasseUtilisateur;
        private System.Windows.Forms.Button buttonSupprimer;
    }
}