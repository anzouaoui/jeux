﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Jeux.Vues;

namespace Jeux.vues
{
    public partial class FormFenetrePrincipale : Form
    {
        static string pseudo;
        Form Mdichild;

        public FormFenetrePrincipale()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Jouer au morpion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonMorpion_Click(object sender, EventArgs e)
        {
            Mdichild = new Jeux.Vues.Morpion.FormMenuMorpion();
            Mdichild.Show();
            this.Hide();
        }

        /// <summary>
        /// Jouer au Pendu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPendu_Click(object sender, EventArgs e)
        {
            Mdichild = new Jeux.Vues.Pendu.FormPendu();
            Mdichild.Show();
            this.Hide();
        }

        /// <summary>
        /// Jouer au labyrinthe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLabyrinthe_Click(object sender, EventArgs e)
        {
            //Mdichild = new FormLabyrinthe();
            Mdichild.Show();
            this.Hide();
        }

        /// <summary>
        /// jouer au chiffre mystere
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPingPong_Click(object sender, EventArgs e)
        {
            Mdichild = new Jeux.Vues.Pong.FormPong(); ;
            Mdichild.Show();
            this.Hide();
        }

        private void buttonSnake_Click(object sender, EventArgs e)
        {
            Mdichild = new Jeux.Vues.Snake.FormSerpent();
            Mdichild.Show();
            this.Hide();
        }

        public static void SetPseudo(string pseudoJoueur)
        {
            pseudo = pseudoJoueur;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormFenetrePrincipale_Load(object sender, EventArgs e)
        {
            toolStripTextBoxPseudo.Text = pseudo;
        }

        private void FormFenetrePrincipale_FormClosed(object sender, FormClosedEventArgs e)
        {
            Mdichild = new FormConnexion();
            Mdichild.Show();
            this.Hide();
        }

        private void monCompteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormCompteUtilisateur();
            Mdichild.Show();
            this.Hide();
        }

        private void tableauDesScoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mdichild = new FormMeilleurScore();
            Mdichild.Show();
            this.Hide();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
