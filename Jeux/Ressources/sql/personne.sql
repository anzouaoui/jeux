-- Base de données :  `jeux`
--Auteur: Anouk ZOUAOUI
--Date de création: 23/10/2016

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
  `id_pers` int(11) NOT NULL AUTO_INCREMENT,
  `nom_pers` varchar(50) NOT NULL,
  `pseudo` varchar(100) NOT NULL,
  `mdp_pers` varchar(50) NOT NULL,
  `confMdp` varchar(50) NOT NULL,
  `meilleurScorePong` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pers`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Structure de la table `score`
--

CREATE TABLE IF NOT EXISTS `score` (
	`id_score` int(11) NOT NULL AUTO_INCREMENT,
	`id_pers` int(11) NOT NULL,
	`sc_morpion2Joueur` int(11) NULL,
	`sc_morpionOrdi` int(11) NULL,
	`sc_snake` int(11) NULL,
	`sc_pong` int(11) NULL,
	`tmps_laby` int(11) NULL,
	CONSTRAINT pk_score PRIMARY KEY (`id_score`, `id_pers`),
	CONSTRAINT fk_personne FOREIGN KEY (`id_pers`) REFERENCES personne (`id_pers`)
)  ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
