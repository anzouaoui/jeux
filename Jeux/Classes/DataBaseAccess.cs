﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;


namespace Jeux.Classes
{
    class DataBaseAccess
    {
        //public static IDbConnection Connexion { get; set; }

        //public static IDbDataParameter CodeParam(string paramName, object value)
        //{
        //    IDbCommand commandSql = Connexion.CreateCommand();
        //    IDbDataParameter parametre = commandSql.CreateParameter();
        //    parametre.ParameterName = paramName;
        //    parametre.Value = value;
        //    return parametre;
        //}

        public static MySqlConnection Connexion { get; set; }

        public static MySqlParameter CodeParam(string paramName, object value)
        {
            MySqlCommand commandSql = Connexion.CreateCommand();
            MySqlParameter parametre = commandSql.CreateParameter();
            parametre.ParameterName = paramName;
            parametre.Value = value;
            return parametre;
        }
    }
}
