﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;


namespace Jeux.Classes
{
    class Personnes
    {
        #region Modele objet
        private int id;
        public string Nom { get; set; }
        public string Pseudo { get; set; }
        public string MotDePasse { get; set; }
        public string ConfMotDePasse { get; set; }

        public override string ToString()
        {
            return String.Format("Nom: {0} Pseudo: {1}", Nom, Pseudo);
        }
        #endregion

        public Personnes()
        {
            //Identifie une personne non referencé dans la base
            id = -1;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql = "SELECT id_pers , nom_pers, pseudo ,  mdp_pers , confMdp FROM personne";
        private static string _selectByIdSql = "SELECT id_pers , nom_pers, pseudo ,  mdp_pers , confMdp FROM personne WHERE id_pers = ?id ";
        private static string _updateSql = "UPDATE contact SET nom_pers=?nom, pseudo=?pseudo , mdp_pers=?motDePasse, confMdp=?confMotDePasse  WHERE id_pers=?id ";
        private static string _insertSql = "INSERT INTO personne (nom_pers,pseudo,mdp_pers, confMdp) VALUES (?nom,?pseudo, ?motDePasse, ?confMotDePasse)";
        private static string _deleteByIdSql = "DELETE FROM personne WHERE id_pers = ?id";
        private static string _getLastInsertId = "SELECT id_pers FROM personne WHERE nom_pers = ?nom AND pseudo=?pseudo AND mdp_pers=?motDePasse AND confMdp=?condMotDePasse";
        #endregion

        #region Méthodes d'accès aux données
        /// <summary>
        /// Valorise un objet personne depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="idPersonne">La valeur de la clé primaire</param>
        public static Personnes Fectch(int idPersonne)
        {
            Personnes unePersonne = null;
            //connection à la base de données
            DataBaseAccess.Connexion.Open();
            //Initialisation d'un objet permettant d'interroger la base de données
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            //Définit la requete à utiliser
            commandSql.CommandText = _selectByIdSql;
            //Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?id", idPersonne));
            //Prépare la requête
            commandSql.Prepare();
            //Exécution de la requête
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            //Lecture du premier enregistrement
            bool existEnregistrement = jeuEnregistrements.Read();

            //Si l'enregistrement existe
            if (existEnregistrement)
            {
                //Initialisation de la variable Contact
                unePersonne = new Personnes();
                //Lecture d'un champ de l'enregistrement
                unePersonne.id = Convert.ToInt32(jeuEnregistrements["id_pers"].ToString());
                unePersonne.Nom = jeuEnregistrements["nom_pers"].ToString();
                unePersonne.Pseudo = jeuEnregistrements["pseudo"].ToString();
                unePersonne.MotDePasse = jeuEnregistrements["mdp_pers"].ToString();
                unePersonne.ConfMotDePasse = jeuEnregistrements["confMdp"].ToString();
            }
            //fermeture de la connexion
            DataBaseAccess.Connexion.Close();
            return unePersonne;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un contact dans la base de données
        /// </summary>
        public void Save()
        {
            if (id == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime la personne représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?id", id));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            id = -1;
        }

        /// <summary>
        /// Modifie la personne représenté par l'instanct courante dans le SGBD
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?id", id));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nom", Nom));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?pseudo", Pseudo));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?motDePasse", MotDePasse));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?confMotDePasse", ConfMotDePasse));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Insert une nouvelle personne dans le SGBD
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nom", Nom));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?pseudo", Pseudo));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?motDePasse", MotDePasse));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?confMotDePasse", ConfMotDePasse));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?nom", Nom));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?pseudo", Pseudo));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?motDePasse", MotDePasse));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?confMotDePasse", ConfMotDePasse));
            commandGetLastId.Prepare();
            IDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                id = Convert.ToInt16(jeuEnregistrements["id_pers"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les contacts
        /// </summary>
        /// <returns>Une collection de contacts</returns>
        public static List<Personnes> FetchAll()
        {
            List<Personnes> resultat = new List<Personnes>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            IDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Personnes personne = new Personnes();
                string idPersonne = jeuEnregistrements["id_pers"].ToString();
                personne.id = Convert.ToInt32(idPersonne);
                personne.Nom = jeuEnregistrements["nom_pers"].ToString();
                personne.Pseudo = jeuEnregistrements["pseudo"].ToString();
                personne.MotDePasse = jeuEnregistrements["mdp_pers"].ToString();
                personne.ConfMotDePasse = jeuEnregistrements["confMdp"].ToString();
                resultat.Add(personne);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;
        }

        #endregion
    }
}
